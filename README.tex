\documentclass[]{article}

\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[normalem]{ulem}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{minted}

% just a comment 

\setminted{fontsize=\small}
% calling an extension in the preambule
% initializes this extension with arguments
% see extension section for more details.


\begin{document}

\author{mdslides}
\date{\today}

\title{Generate slides \sout{easily}}
\maketitle

\begin{abstract}
mdslides allows to rapidly make Beamer slides or LaTeX article with
beatiful equations and unleash all power of LaTeX. 
\end{abstract}

\section{Installation}

\medskip\noindent
\textbf{Installation}\hfill\break
\begin{minted}[fontsize=\footnotesize]{sh}
### install a latex compiler, e.g., for ubuntu:
sudo apt-get install texlive texlive-xetex
### install mdslides
pip install git+https://gitlab.com/mborisyak/mdslides.git

### alternatively, clone the repository
git clone git@gitlab.com:mborisyak/mdslides.git
cd mdslides/ && pip install -e .
\end{minted}

\texttt{mdslides} exports two scripts: \texttt{mdslides} and \texttt{mdarticle},
that should be appended to \texttt{\$PATH}.


Alternatively, \texttt{mdslides} can be invoked as:
\begin{minted}[]{sh}
python -m mdslides.mdslides <source>
python -m mdslides.mdarticle <source>
\end{minted}


\section{Getting Started }
% If this looks too weird for a normal README,
% try:
% mdslides README.md
% or
% mdarticle README.md 
% and check out README.pdf
% Also, precompiled README.pdf included in the repository.


\medskip\noindent
\textbf{The goal}\hfill\break
\texttt{mdslides} uses markdown to produce latex/beamer code.

It is designed for rapid creation of slides or articles:
\begin{itemize}
  \item conventional markdown markup covers 90\%  of frequently used \LaTeX features;
  \item extended language of mdslides covers 90\%  of the rest;
\begin{itemize}
  \item e.g., mdslides allows to insert arbitrary \LaTeX~code, including math;
\end{itemize}

  \item extensions further add to mdslides capabilities.
\end{itemize}

\texttt{mdslides} \textbf{does not} aim to replicate how rendered markdown documents looks ---
\texttt{.md} files are treated as source code, however, the syntax closely follows conventional markdown.


\medskip\noindent
\textbf{\texttt{mdslides} and \texttt{markdown}}\hfill\break
\texttt{mdslides} uses \textbf{subset} of \texttt{markdown} syntax and \textbf{superset} of markdown semantics:
\begin{itemize}
  \item a particular subset of markdown is interpreted by \texttt{mdslides} almost identically semantically;
  \item not all (but almost all) proper markdown files are accepted by \texttt{mdslides}:
\begin{itemize}
  \item for example: \texttt{mdslides} accepts only 
    \href{https://daringfireball.net/projects/markdown/syntax\#header}{non-closed atx-style headers}
\end{itemize}

  \item semantics (markup) is not always preserved, for example:
\begin{itemize}
  \item in \texttt{markdown}, both \_text\_ and *text* produce \textit{text} (in an italic font);
  \item \texttt{mdslides} interprets \_text\_ as {\textbackslash}texttt (monospace font);
\end{itemize}

  \item overall, \texttt{mdslides} source files look okay when parsed by \texttt{markdown}.
\end{itemize}


\medskip\noindent
\textbf{Slides and articles}\hfill\break
\texttt{mdslides} offers two renderers:
\begin{itemize}
  \item beamer mode, corresponds to \texttt{mdslides} script;
  \item article mode, corresponds to \texttt{mdarticle} script.
\end{itemize}

Differences between these modes are mostly in handling \textit{subsections} (defined by \#\#\#):
\begin{itemize}
  \item beamer mode threats them as slides;
  \item article mode threats them as paragraphs.
\end{itemize}


\medskip\noindent
\textbf{Document structure}\hfill\break
\texttt{mdslides} uses 3 levels of document structure:
\begin{itemize}
  \item \# specifies the title of the presentation:
\begin{itemize}
  \item must appear only once;
  \item must appear strictly at the beginning of the source file;
  \item the title is supplied to the template file as \texttt{title} variable (see section on themes).
\end{itemize}

  \item \#\# denotes section;
  \item \#\#\# denotes:
\begin{itemize}
  \item title of a slide in the beamer mode (\texttt{mdslides});
  \item paragraph in the article mode (\texttt{mdarticle}).
\end{itemize}

\end{itemize}

\medskip\noindent
\textbf{Document structure}\hfill\break
An example of a typical structure of a slide presentation:

\begin{minted}[]{md}
# Big presentation

## The first section

### The first proper slide
<some text>

### The second proper slide
<some pictures>
\end{minted}


\medskip\noindent
\textbf{Span elements}\hfill\break
\begin{itemize}
  \item \texttt{[example](https://example.org)}
  generates a link: \href{https://example.org}{example} (click it);
  \item \texttt{\_some text\_} becomes \texttt{{\textbackslash}texttt\{some text\}}, e.g. \texttt{some text};
  \item \texttt{\_\_some text\_\_} becomes \texttt{{\textbackslash}underline\{some text\}} e.g. \underline{some text};
  \item \texttt{*some text*} becomes {\texttt{\textbackslash}textit\{some text\}}, e.g. \textit{some text};
  \item \texttt{**some text**} becomes \texttt{{\textbackslash}textbf\{some text\}}, e.g. \textbf{some text};
  \item \texttt{{\textasciigrave}some code{\textasciigrave}} inserts \texttt{some code} as it is directly into latex:
\begin{itemize}
  \item \texttt{{\textasciigrave}{\textbackslash}LaTeX{\textasciigrave}} becomes \LaTeX
\end{itemize}

\end{itemize}

\medskip\noindent
\textbf{Math}\hfill\break
\begin{itemize}
  \item \texttt{\$x{\textasciicircum}2 = {\textbackslash}log y\$} works just like in LaTeX: $x^2 =\log y$;
  \item the same for \$\$, e.g. \texttt{\$\$x{\textasciicircum}2 = {\textbackslash}log y\$\$}:
  $$x^2 =\log y$$
\end{itemize}

Alternatively, one can use blocks for math, e.g.:
\begin{minted}[fontsize=\scriptsize]{md}
~~~equation*
  \mathcal{L}(f) = \frac{1}{N} \sum^N_{i = 1} y_i \log(f(x_i));
~~~
\end{minted}
becomes:
\begin{equation*}
  \mathcal{L}(f) = \frac{1}{N} \sum^N_{i = 1} y_i \log(f(x_i));
\end{equation*}


\medskip\noindent
\textbf{Lists}\hfill\break
Lists work just like in markdown

\begin{minted}[]{md}
List of stuff:
- stuff;
- thing;
- something;
\end{minted}

becomes:

List of stuff:
\begin{itemize}
  \item stuff;
  \item thing;
  \item something;
\end{itemize}


\medskip\noindent
\textbf{Lists}\hfill\break
\begin{minted}[]{md}
My least favorite things:
999. writing documentation;
99. understanding old code I had not documented;
18. sea food;
\end{minted}

becomes:

My least favorite things:
\begin{enumerate}
  \item writing documentation;
  \item understanding old code I had not documented;
  \item sea food;
\end{enumerate}


\medskip\noindent
\textbf{Images}\hfill\break
Similar to markdown:

\texttt{
  \scriptsize
  ![width=1](http://imgs.xkcd.com/comics/self\_description.png)
}

results in:

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/img_403295218.png}
\end{figure}



\medskip\noindent
\textbf{Images}\hfill\break
Notice, that alternative text in the markdown expression for image is replaced by \texttt{arguments}:

\texttt{
  ![<arguments>](<url or path>)
}

\texttt{url or path} must be either:
\begin{itemize}
  \item a valid path to an image on a local filesystem:
\begin{itemize}
  \item path must be relative to the root directory (the one containing \texttt{.md} file);
\end{itemize}

  \item a valid url (in this case image will be downloaded).
\end{itemize}


\medskip\noindent
\textbf{Images}\hfill\break
Images accept the following arguments:
\begin{itemize}
  \item \texttt{width, height : number or length}:
\begin{itemize}
  \item passed to \texttt{{\textbackslash}includegrephics} as ... \texttt{width} and \texttt{height};
  \item if an integer or a real number, e.g., \texttt{width=1}, width/height is multiplied by \texttt{{\textbackslash}textwidth} or \texttt{{\textbackslash}textheight}. 
\end{itemize}

  \item \texttt{nofigure}:
\begin{itemize}
  \item \texttt{mdslides} automatically wraps \texttt{{\textbackslash}includegrephics} into a figure, this flag prevents that;
\end{itemize}

  \item \texttt{caption : string}:
\begin{itemize}
  \item adds \texttt{{\textbackslash}caption}.
\end{itemize}

\end{itemize}

\medskip\noindent
\textbf{Footnotes}\hfill\break
\texttt{[<ref>]}\footnote{Footnote reference can not appear at the beginning of a line.} and \texttt{[<ref>]: <text>}\footnote{Footnote definition \textbf{must} appear at the beginning of a line.} inserts \texttt{{\textbackslash}footnote}\footnote{Footnotes are handled by an extension, this behaviour can be overridden in favor of, e.g. \texttt{{\textbackslash}cite} (see \texttt{references} extension).}, e.g.:

\begin{minted}[]{md}
foot[note]

[note]: about footnotes
\end{minted}

becomes:

foot\footnote{about footnotes}






\medskip\noindent
\textbf{Slide structure\footnote{this only applies to \texttt{beamer} mode, article mode simply ignores horizontal rules.}}\hfill\break
Markdown's horizontal rules are used by \texttt{mdslides} to define slide structure:
\begin{itemize}
  \item *** (three stars) defines vertical split;
  \item \textendash\textendash\textendash (three hyphens) defines horizontal split.
\end{itemize}

Spacing between stars and hyphens specifies priority:
rules with largest number of spaces between the symbols have higher priority.

\textit{Note:} horizontal split technically does nothing, except for defining limits of vertical splits.



\medskip\noindent
\textbf{Slide structure, an example}\hfill\break
The next slide is generated with the following layout:

\begin{minted}[]{md}
<upper left>
***
<upper right>

- - -

<lower>
\end{minted}

Since - - - has more spaces in between the symbols, it has higher priority.


\medskip\noindent
\textbf{A slide}\hfill\break
A slide with text:
\begin{itemize}
  \item and a list;
  \item 2-item list.
\end{itemize}
And another text:
\begin{enumerate}
  \item width a list!
  \item numbered one!
  \item And more items than in the right one!
\end{enumerate}
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/img_324008321.png}
\end{figure}



\medskip\noindent
\textbf{Slide structure, an example}\hfill\break
Changing priority changes layout:

\begin{minted}[]{md}
<left>

* * *

<upper right>
---
<lower right>
\end{minted}

Now, vertical split (* * *) has higher priority.


\medskip\noindent
\textbf{A slide}\hfill\break
A slide with text:
\begin{itemize}
  \item and a list;
  \item 2-item list.
\end{itemize}
And another text:
\begin{enumerate}
  \item width a list!
  \item numbered one!
  \item And more items than in the right one!
\end{enumerate}
\begin{figure}
\centering
\includegraphics[width=1\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/img_443676900.png}
\end{figure}



\section{Compilation}

\medskip\noindent
\textbf{Compilation}\hfill\break
The command:
\begin{minted}[]{sh}
bash bin/mdslides README.md
\end{minted}

executed from \texttt{mdslides}' root directory outputs compiled \texttt{README.pdf}.

For the full list of options, try:
\begin{minted}[]{sh}
bash bin/mdslides --help
\end{minted}

It is also might be convenient to add:
\begin{minted}[]{sh}
export PATH="$PATH:<mdslides root>/bin"
\end{minted}
to your \texttt{.bashrc}:


\section{\LaTeX, blocks and more}

\medskip\noindent
\textbf{Blocks and code blocks}\hfill\break
Markdown provides 2 ways for inserting code:
\begin{itemize}
  \item via triple tildes \textasciitilde\textasciitilde\textasciitilde,
  \item via triple "graves" \textasciigrave\textasciigrave\textasciigrave.
\end{itemize}

\texttt{mdslides} distinguishes between these:
\begin{itemize}
  \item text surrounded by triple tildes is called \textbf{block} and is interpreted as \LaTeX~code;
  \item text surrounded by triple "graves" is called \textbf{code block} and denotes a call to an extension. 
\end{itemize}


\medskip\noindent
\textbf{Blocks}\hfill\break
\begin{center}
  \Large
  curly block = \LaTeX~code 
\end{center}

The full block syntax\footnote{Arguments are optional --- if not specified, \LaTeX{} arguments are omitted as well.}:
\begin{minted}[]{md}
~~~<env>[<arguments>][<optional arguments>]
<body>
~~~
\end{minted}
expands into:
\begin{minted}[]{latex}
\begin{<env>}[<optional arguments>]{<arguments>}
<body>
\end{<env>}
\end{minted}
\vspace{3mm}


\medskip\noindent
\textbf{Blocks, examples}\hfill\break
\begin{minted}[]{md}
~~~tabular['|c | c|']
method & ROC AUC;\\
\hline
method 1 & 0.99;\\
\hline
 method 2 & 0.91;\\
\hline
~~~
\end{minted}

generates:

\begin{tabular}{{|c | c|}}
\hline
method & ROC AUC;\\
\hline
method 1 & 0.99;\\
\hline
 method 2 & 0.91;\\
\hline
\end{tabular}


\medskip\noindent
\textbf{Blocks, examples}\hfill\break
\begin{minted}[]{md}
~~~center
\textbf{
  \Large
  Some text!
}
~~~
\end{minted}

generates:

\begin{center}
\textbf{
  \Large
  Some text!
}
\end{center}


\medskip\noindent
\textbf{Blocks, examples}\hfill\break
\begin{minted}[]{md}
~~~eqnarray*
f(x) &=& \sigma\big(g(x)\big);\\
g(x) &=& W \cdot x + b.
~~~
\end{minted}

generates:

\begin{eqnarray*}
f(x) &=& \sigma\big(g(x)\big);\\
g(x) &=& W \cdot x + b.
\end{eqnarray*}


\medskip\noindent
\textbf{Blocks, examples}\hfill\break
\begin{minted}[]{md}
~~~minted[python][fontsize=\large]
def inc(x):
    return x + 1
~~~
\end{minted}

expands into (replace \texttt{x} with \texttt{minted}):

\begin{minted}[]{latex}
\begin{x}[fontsize=\large]{python}
def inc(x):
    return x + 1
\end{x}
\end{minted}

which generates:

\begin{minted}[fontsize=\large]{python}
def inc(x):
    return x + 1
\end{minted}


\medskip\noindent
\textbf{Code blocks}\hfill\break
\begin{center}
  \Large
  grave block = extension call 
\end{center}

The full code-block syntax \footnote{Arguments are optional.}:
\begin{minted}{md}
```<func>[<arguments>]
<body>
```
\end{minted}

calls an extension that accepts \texttt{<func>} passing \texttt{<arguments>} and \texttt{<body>} to the python code of the extension.

God only knows, what these extensions do\footnote{For the forbidden fruit, see \texttt{extensions} section.}.




\medskip\noindent
\textbf{Blocks}\hfill\break
\begin{minted}{md}
```<func>[<arguments>]
<body>
```
\end{minted}

If no \texttt{<func>} is specified, the code is inserted as is, e.g.:

\begin{minted}{md}
```
\begin{center}
\textbf{\Large some text!}
\end{center}
```
\end{minted}

produces:

\begin{center}
\textbf{\Large some text!}
\end{center}


\medskip\noindent
\textbf{Minted extension}\hfill\break
\href{https://www.ctan.org/pkg/minted}{\texttt{minted} extension} is the lowest level extension that accepts all \texttt{<func>}, thus,
allowing \texttt{mdslides} to emulate standard behavior of markdown, if \texttt{<func>} is not intercepted by any other extension:

\begin{minted}{md}
```python
def f(x):
    return x + 1
```
\end{minted}

is actually a call to \texttt{minted} extension:
\begin{minted}[]{python}
def f(x):
    return x + 1
\end{minted}


\section{Preamble, environments and drama}

\medskip\noindent
\textbf{Preamble}\hfill\break
Block and code blocks receive a special treatment when placed after the title, but \textbf{before} any section:
\begin{itemize}
  \item blocks define template variables;
  \item code blocks configure extensions.
\end{itemize}

A special code block called \texttt{preambule} allows to configure \texttt{{\textbackslash}documentclass\{beamer\}} class command:
\begin{itemize}
  \item arguments of \texttt{preambule} are passed directly to \texttt{{\textbackslash}documentclass\{beamer\}};
  \item \texttt{preambule} body is inserted before \texttt{{\textbackslash}begin\{document\}}.
\end{itemize}


\medskip\noindent
\textbf{Preamble example}\hfill\break
\begin{minted}{md}
# The title

```preambule[ascpectratio=169, 12pt]
\DeclareMathOperator*{\argmin}{\mathrm{arg\,min}}
\DeclareMathOperator*{\argmax}{\mathrm{arg\,max}}
```
\end{minted}

expands into:
\begin{minted}[]{latex}
\documentclass[ascpectratio=169, 12pt]{beamer}
<package declaration>
\DeclareMathOperator*{\argmin}{\mathrm{arg\,min}}
\DeclareMathOperator*{\argmax}{\mathrm{arg\,max}}
\end{minted}



\medskip\noindent
\textbf{Environments}\hfill\break
Each template file contains "placeholders"/environment variables (e.g., \texttt{\$title}),
which are filled with latex code upon parsing markdown. Placing blocks before the first section
allows to add custom code to environment variables, e.g.:
\begin{minted}[]{md}
~~~foreword
\author{A. N. Onymous}
~~~ 
\end{minted}
appends \texttt{{\textbackslash}author\{A. N. Onymous\}} to the \texttt{foreword} variable\footnote{See \texttt{info} extension for an alternative way of specifying authors etc.}. 



\medskip\noindent
\textbf{Environments}\hfill\break
List of environment variables depends on template file, but, usually, the following variables are available:
\begin{itemize}
  \item \texttt{preamble} --- special variable, see above;
  \item \texttt{foreword} --- appears after \texttt{{\textbackslash}begin\{document\}}, but before the command for generating title;
  \item \texttt{preface} --- appears after title, but before any content, absent in beamer templates;
  \item \texttt{afterword} --- appears after all content.
\end{itemize}

For examples, \texttt{{\textbackslash}author\{A. N. Onymous\}} command
should be placed into the \texttt{foreword} because author's name is used for generating title. 


\medskip\noindent
\textbf{Extension initialization}\hfill\break
\begin{center}
  \Large
  before the first section = \texttt{\_\_init\_\_}\\
  after the first section = \texttt{\_\_call\_\_}\\
\end{center}

A code block with an name of an extension initializes that extension:
\begin{minted}{md}
# Title
```superextension[arg1=1, arg2, arg3=string]
some text
```
## The first section
\end{minted}


\medskip\noindent
\textbf{Extension initialization}\hfill\break
More precisely:

\begin{minted}{md}
# Title
```superextension[arg1=1, arg2, arg3=string]
some text
```
## The first section
\end{minted}

calls initialization method of \texttt{superextension}:
\begin{minted}[fontsize=\small]{python}
Extension = available_extensions['superextension']
extension = Extension(
  'some text',
  **dict(arg1=1, arg2=None, arg3='string')
)
\end{minted}


\medskip\noindent
\textbf{Extension initialization, example}\hfill\break
\texttt{abstract} extension puts \texttt{{\textbackslash}begin\{abstract\}<text>{\textbackslash}end\{abstract\}}
into the correct environment variable (\texttt{preface}):

\begin{minted}{md}
# Title
```abstract
Lorem ipsum dolor sit amet.
```
## The first section
\end{minted}

initializes \texttt{abstract}  extension with the text, \texttt{abstract} simply memorizes it,
and append it into the \texttt{preface} variable.


\medskip\noindent
\textbf{Extension initialization, example}\hfill\break
Note, that name of an extension might not coincide with names of the functions it accepts.
For example, \texttt{minted} extension accepts all functions, but can be configured only via:
\begin{minted}{md}
# Title
```minted[fontsize=\Large]
```
## The first section
\end{minted}


\medskip\noindent
\textbf{Extension initialization, example}\hfill\break
The following code raises an error as there is no extension named \texttt{python}:
\begin{minted}{md}
# Title
```python[fontsize=\Large]
f = lambda x: x + 1 
```
## The first section
\end{minted}


\medskip\noindent
\textbf{Extension initialization, example}\hfill\break
However, placed after the first section,
this code block is interpreted as \texttt{minted} extension call,
thus, successfully renders as \texttt{python} code with \texttt{fontsize={\textbackslash}Large}:

\begin{minted}{md}
# Title
## The first section
```python[fontsize=\Large]
f = lambda x: x + 1
```
\end{minted}

\begin{minted}[fontsize=\Large]{python}
f = lambda x: x + 1
\end{minted}


\section{Extensions}

\medskip\noindent
\textbf{\texttt{minted}}\hfill\break
\href{https://www.ctan.org/pkg/minted}{\texttt{minted} extension} extension inserts
formatted and highlighted code.

This extension accepts all functions, but has the lowest priority,
thus catches all functions not accepted by other extensions.

Alternatively, \texttt{minted} can be called as:

\begin{minted}{md}
```minted[lang=python]
f = lambda x: x + 1
```
\end{minted}

\begin{minted}[]{python}
f = lambda x: x + 1
\end{minted}


\medskip\noindent
\textbf{\texttt{minted}}\hfill\break
Arguments passed to \texttt{minted} extension during
initialization are wrapped into \texttt{{\textbackslash}setminted\{k=v\}}
and inserted into preamble:

\begin{minted}{md}
# Title
```minted[fontsize=\Large]
```
## The first section
\end{minted}

produces:
\begin{minted}[]{md}
\setminted{fontsize=\Large}
\end{minted}


\medskip\noindent
\textbf{\texttt{info}}\hfill\break
\texttt{info} parses information in form of:
\begin{minted}{md}
```info
author: A. N. Onymous;
subtitle = just an example
date = \today
```
\end{minted}

into:

\begin{minted}[]{latex}
\author{A. N. Onymous}
\subtitle{just an example}
\date{\today}
\end{minted}

and insert it into \texttt{foreword} environment variable.


\medskip\noindent
\textbf{\texttt{abstract}}\hfill\break
When initialized with a text,
\texttt{abstract} puts \texttt{{\textbackslash}begin\{abstract\}<text>{\textbackslash}end\{abstract\}}
into \texttt{preface}:

\begin{minted}{md}
# Title
```abstract
Lorem ipsum dolor sit amet.
```
## The first section
\end{minted}

Note that \texttt{preface} is absent from beamer templates\footnote{unless, of course, you put it there.},
thus has no effect (like in these slides).



\medskip\noindent
\textbf{\texttt{footnotes}}\hfill\break
Handles footnotes, can be overridden by \texttt{references}.
Does not accept any arguments during initialization.
That it\ldots


\medskip\noindent
\textbf{\texttt{references}}\hfill\break
Overrides behavior of markdown references (not default extension).

With \texttt{references} turned on, markdown references:
\begin{minted}[]{md}
some statement[footnote]
[footnote]: some explanation 
\end{minted}
now generate citations instead of footnotes.


\medskip\noindent
\textbf{\texttt{references}}\hfill\break
Arguments:
\begin{itemize}
  \item \texttt{bibliography: bool} --- if set the extension generates list of
  references via \texttt{thebibliography}, False by default.
  \item \texttt{style: string} --- if \texttt{bibliography=False} sets template for citation symbol,
  by default numbers surrounded by square brackets, e.g., \texttt{[13]}.
  \item \texttt{afterword: bool} --- if set, extension automatically inserts generated references
    into \texttt{afterword} environment variable, True by default.
\end{itemize}


\medskip\noindent
\textbf{\texttt{graphviz}}\hfill\break
Support for the \href{https://www.graphviz.org/}{graphviz} language.
\texttt{graphviz} executable should be installed.

\begin{minted}{md}
```graphviz[width=1]
digraph G {
  rankdir = LR;
  markdown -> latex [label="mdslides"];
  latex -> pdf [label="xelatex"]
}
```
\end{minted}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/dot-graph-891046795.pdf}
\end{figure}


\medskip\noindent
\textbf{\texttt{graphviz}}\hfill\break
\texttt{graphviz} accepts the same arguments as images, and additionally:
\begin{itemize}
  \item \texttt{layout: string} --- corresponds to graphviz layout (e.g., \texttt{dot}, \texttt{neato} etc), default: \texttt{dot};
  \item \texttt{format: string} --- \href{https://graphviz.gitlab.io/_pages/doc/info/output.html}{format}
  of the resulting image, default: \texttt{pdf};
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=1\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/dot-graph-326658399.pdf}
\caption{An informative caption.}
\end{figure}


\medskip\noindent
\textbf{TIKZ}\hfill\break
Inserts \texttt{tikz} as a picture. By default wraps tikz picture into \texttt{figure}.

Accepts:
\begin{itemize}
  \item \texttt{nofigure} flag;
  \item \texttt{caption: string} argument
  \item other arguments are passed into \texttt{tikz}.
\end{itemize}
\begin{figure}[!h]
\centering
\begin{tikzpicture}[scale=1.5]
  \draw [blue, domain=0.001:1.5] plot (\x, {-\x * ln(\x)});
  \draw [thick, ->] (-0.5, 0) -- (2, 0) node[anchor=north west] {$x$};
  \draw [thick, ->] (0, -1) -- (0, 1) node[anchor=south east] {$f$};
  \node at (1, 1) {$f(x) = -x \log x$};

\end{tikzpicture}

\caption{{$f(x) = -x \log x$ with tikz!}}
\end{figure}

\section{Available themes}

\medskip\noindent
\textbf{Metropolis}\hfill\break
There are 2 derivatives of \href{https://github.com/matze/mtheme/}{Metropolis theme} available:
\begin{itemize}
  \item \texttt{metropolis}:
\begin{itemize}
  \item mostly original metropolis theme;
\end{itemize}

  \item \texttt{msimple}:
\begin{itemize}
  \item simplistic black and white versions.
\end{itemize}

\end{itemize}\begin{figure}
\centering
\includegraphics[width=1\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/img_1017292982.png}
\end{figure}




\medskip\noindent
\textbf{Metropolis License}\hfill\break
The theme itself is licensed under a \href{http://creativecommons.org/licenses/by-sa/4.0/}{Creative Commons Attribution-ShareAlike
4.0 International License}. This
means that if you change the theme and re-distribute it, you \textit{must} retain the
copyright notice header and license it under the same CC-BY-SA license. This
does not affect the presentation that you create with the theme.


\medskip\noindent
\textbf{Simple}\hfill\break
If you have followed the instructions you are looking at it right now.

A minimalistic theme by Facundo Muñoz\footnote{This repository contains a slightly modified version.}:
\begin{itemize}
  \item \href{https://github.com/famuvie/beamerthemesimple}{https://github.com/famuvie/beamerthemesimple}
\end{itemize}



\medskip\noindent
\textbf{Zurich}\hfill\break
Clean, colorful theme.
\begin{itemize}
  \item \href{https://github.com/ppletscher/beamerthemezurich}{https://github.com/famuvie/beamerthemesimple}
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/img_857964360.png}
\end{figure}



\medskip\noindent
\textbf{Focus}\hfill\break
Nice, pleasant theme from:
\begin{itemize}
  \item \href{https://github.com/elauksap/focus-beamertheme}{https://github.com/elauksap/focus-beamertheme}
\end{itemize}

As with \texttt{metropolis} theme, 2 versions are available: \texttt{focus} and \texttt{fsimple}\footnote{Well, this theme is also tweaked, as I don't like serif fonts in slides.}. 

\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{/tmp/tmp0c71ioivmdslides/mdslides/img_260589424.png}
\end{figure}




\section{Themes}

\medskip\noindent
\textbf{Template file}\hfill\break
\texttt{template.tex} is used as a \dots template for beamer or latex\footnote{\texttt{mdslides} uses \href{https://docs.python.org/3/library/string.html\#template-strings}{string.Template} internally.}:
\begin{itemize}
  \item \texttt{mdslides} parses input markdown;
  \item computes environment variables;
  \item these variables are inserted into the template.
\end{itemize}

The following variables are always present:
\begin{itemize}
  \item \texttt{\$packages} expands into series of \texttt{{\textbackslash}usepackage\{<package>\}};
  \item \texttt{\$preamble} expands into preamble;
  \item \texttt{\$title} expands into whatever comes after the single \#;
  \item \texttt{\$document} expands into the content of the document.
\end{itemize}



\medskip\noindent
\textbf{Template file}\hfill\break
Other environment variables are typically present in template files:
\begin{itemize}
  \item \texttt{foreword} --- appears after \texttt{{\textbackslash}begin\{document\}}, but before the command for generating title;
  \item \texttt{preface} --- appears after title, but before any content, absent in beamer templates;
  \item \texttt{afterword} --- appears after all content.
\end{itemize}

See \texttt{template.tex} in any of the available themes for example.


\medskip\noindent
\textbf{Make your own theme}\hfill\break
\begin{enumerate}
  \item put theme files into \texttt{mdslides/themes/theme-name} directory;
  \item don't forget to include \texttt{template.tex};
  \item compile with \texttt{mdslides -t theme-name ...}
  \item tweak fonts...
  \item tweak colors...
  \item crank up drama.
  \item repeat...
\end{enumerate}

You can also put theme files in the same directory as the source markdown file.



\end{document}
