import re

from ..parser import ArgumentList, String

__all__ = [
  'split_by',
  'split_first',
  'convert_arguments',
  'render_arguments',
  'pop_argument'
]

def split_by(xs, sep_predicate):
  head = None
  tail = list()

  group = list()
  sep = None

  for x in xs:
    if sep_predicate(x):
      if sep is None:
        head = group
      else:
        tail.append((sep, group))

      sep = x
      group = list()
    else:
      group.append(x)

  if sep is None:
    head = group
  else:
    tail.append((sep, group))

  return head, tail

def split_first(xs, predicate):
  for i, x in enumerate(xs):
    if predicate(x):
      return xs[:i], xs[i:]

  return xs, list()

def convert_arguments(args : ArgumentList):
  if args is None:
    return dict()

  kwargs = dict()

  for arg in args:
    key = arg.key
    value = arg.value if arg.value is not None else True

    if isinstance(value, String):
      value = value.value

    if key in kwargs:
      raise Exception('argument %s (=%s) is specified twice' % (key, value))

    kwargs[key] = value

  return kwargs

def render_arguments(args : ArgumentList):
  results = []

  for arg in args:
    key = arg.key

    if arg.value is None:
      results.append(key)
    else:
      if isinstance(arg.value, String):
        value = arg.value.value
      else:
        value = str(arg.value)

      results.append('%s=%s' % (key, value))

  return ', '.join(results)

def pop_argument(args : ArgumentList, key, default_value=None, default=None):
  positions = [
    i for i, arg in enumerate(args)
    if arg.key == key
  ]

  if len(positions) > 0:
    pos = positions[0]
    arg = args[pos]
    return (default_value if arg.value is None else arg.value), ArgumentList(args[:pos] + args[(pos + 1):])
  else:
    return default, args

