from string import Template

from ..parser import *
from ..extensions import *

from .utils import *

__all__ = [
  'Generator'
]

def _pool_variable(template):
  import re
  r = re.compile('(?<!\\w|\\$)\\$(?P<value>\\w+)')
  return r.findall(template)

class Generator(object):
  def __init__(self, renderer, template, theme=None, working_dir='./'):
    self._template = template
    self._theme = theme
    self._working_dir = working_dir
    self._renderer = renderer

  def __call__(self, parsed : Document):
    env_vars = _pool_variable(self._template)

    preamble, rest = split_first(
      parsed,
      predicate=lambda x: isinstance(x, Header) and len(x.level.strip()) >= 2
    )

    presection, sections = split_by(
      rest,
      sep_predicate=lambda x: isinstance(x, Header) and len(x.level.strip()) == 2
    )

    if len(presection) > 0:
      presection = (Header(level=2, body=None), presection)
      sections = [presection] + sections
    else:
      pass

    title, preamble, config, environment, additional_preambule = self.render_definitions(preamble)

    ### extension initialization
    ### default extensions have lowest priority
    default_extensions = self._renderer.default_extensions()
    extension_names = [
      ext_name
      for ext_name in config
      if ext_name not in default_extensions
    ] + default_extensions

    extensions = []
    for ext_name in extension_names:
      if ext_name not in available_extensions:
        raise Exception('There is no such extension: %s' % (ext_name, ))

      ext = available_extensions[ext_name]
      ext_obj = config.get(ext_name, None)
      if ext_obj is not None:
        extensions.append((ext, ext_obj.body, sections, ext_obj.args))
      else:
        extensions.append((ext, '', sections, ArgumentList([])))

    renderer = self._renderer(extensions, self._working_dir)

    env = dict()
    env['title'], env['document'] = renderer(title, sections)

    env['packages'] = '\n'.join([
      self.render_package(package)
      for package in renderer.packages()
    ])

    if preamble is None or preamble.args is None:
      env['arguments'] = ''
    else:
      env['arguments'] = render_arguments(preamble.args)

    assert getattr(preamble, 'opt_args', None) is None, 'preamble does not support optional arguments'

    preamble = [preamble.body] if preamble is not None else []
    preamble = preamble + [
      obj.body for obj in additional_preambule
    ]

    additional_environment = dict()
    additional_environment['preamble'] = preamble

    for k, v in environment.items():
      assert getattr(v, 'args', None) is None, 'environment variables do not support arguments'
      assert getattr(v, 'opt_args', None) is None, 'environment variables do not support arguments'

      if k not in additional_environment:
        additional_environment[k] = [v.body]
      else:
        additional_environment[k].append(v.body)

    for ext in renderer.extensions:
      for k, v in ext.environment().items():
        if k not in additional_environment:
          additional_environment[k] = [v]
        else:
          additional_environment[k].append(v)

    for k in additional_environment:
      assert k not in env, 'attempt to redefine %s' % (k, )
      env[k] = '\n'.join(additional_environment[k])

    for k in env_vars:
      if k not in env:
        import warnings
        warnings.warn('Template variable %s was not defined' % (k, ))
        env[k] = ''

    return Template(self._template).substitute(env)

  def render_package(self, package):
    if isinstance(package, tuple):
      name, options = package
      return '\\usepackage[%s]{%s}' % (options, name)
    else:
      return '\\usepackage{%s}' % (package,)

  def render_definitions(self, definitions):
    assert isinstance(definitions[0], Header) and len(definitions[0].level.strip()) == 1, \
      'The first element must be a title (single #).'

    title = definitions[0].body
    preamble = None
    config = dict()
    environment = dict()
    additional_preambule = list()

    for obj in definitions[1:]:
      if isinstance(obj, (Code, Block)) and obj.func == 'preamble':
        if preamble is not None:
          raise Exception('preamble is specified twice')
        preamble = obj

      elif isinstance(obj, Code) and obj.func is not None:
        if obj.func in config:
          raise Exception('Extension %s was configured twice' % (obj.func, ))
        config[obj.func] = obj

      elif isinstance(obj, Block) and obj.func is not None:
        if obj.func in environment:
          raise Exception('Environment variable %s was defined twice' % (obj.func,))
        environment[obj.func] = obj

      elif isinstance(obj, (Code, Block)):
        additional_preambule.append(obj)
      elif isinstance(obj, RichText) and len(obj.value) == 1 and len(obj.value[0].strip()) == 0:
        continue
      else:
        raise Exception(
          'code %s is not understood:\n'
          '- if it is text, move it after the first section,\n'
          '- if it is a part of preamble or extension initialization, wrap it into code block (i.e. with ```),\n'
          '- if it is an environment variable, wrap it into block (i.e. with ~~~)' % (obj,)
        )

    return title, preamble, config, environment, additional_preambule
