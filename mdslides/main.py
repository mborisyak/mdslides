from .parser import document
from .generator import Generator
from .compiler import Compiler
from .renderer import renderers

import shutil as sh
import tempfile
import os

__all__ = [
  'main'
]

LATEX_COMPILERS = [
  'xelatex',
  'lualatex',
  'pdflatex'
]

def default_themes_root():
  try:
    import mdslidesthemes
    return os.path.dirname(mdslidesthemes.__file__)
  except ImportError:
    this_dir = os.path.dirname(__file__)
    return os.path.join(os.path.dirname(this_dir), 'mdslidesthemes')

def locate_theme(theme, source_dir, themes_root=None):
  if themes_root is None:
    themes_root = default_themes_root()
    print('Using default themes location (%s)' % themes_root)

  theme_dir = os.path.join(themes_root, theme)
  if not os.path.isdir(theme_dir):
    raise Exception('%s is not a directory!' % theme_dir)

  if os.path.isfile(os.path.join(source_dir, 'template.tex')):
    template_path = os.path.join(source_dir, 'template.tex')
    print('Using custom template: %s' % template_path)

  elif os.path.isfile(os.path.join(theme_dir, 'template.tex')):
    template_path = os.path.join(theme_dir, 'template.tex')
    print('Using theme template: %s' % template_path)

  else:
    this_dir = os.path.dirname(__file__)
    template_path = os.path.join(this_dir, '__template.tex')
    print('Using generic template: %s' % template_path)

  return theme_dir, template_path

def main(default_theme, renderer):
  import argparse

  args_parser = argparse.ArgumentParser(description='Converts markdown into beamer slides/LaTeX articles')
  args_parser.add_argument(
    'input', type=str, metavar='input',
    help='input markdown file'
  )
  args_parser.add_argument(
    '-t', '--theme', type=str, default=default_theme, metavar='theme',
    help='specifies theme with which LaTeX code will be rendered. Default: %s' % (default_theme, )
  )
  args_parser.add_argument(
    '-d', '--themes-root', type=str, default=None, metavar='themes_root',
    help='directory to look for themes. Default: <mdslides root>/mdslidesthemes/'
  )
  args_parser.add_argument(
    '-o', '--output', type=str, default='./', metavar='output',
    help='output directory or file. Default: input name with pdf extension.'
  )
  args_parser.add_argument(
    '-c', '--compiler', type=str, default=None, metavar='compiler',
    help='specifies LaTex compiler. Default: auto from %s' % (', '.join(LATEX_COMPILERS), )
  )
  args_parser.add_argument(
    '--source', action='store_true',
    help='if set, also outputs generated LaTeX code, useful for debug.'
  )

  args_parser.add_argument(
    '--parsed', action='store_true',
    help='if set, also outputs parsed markdown.'
  )

  args = args_parser.parse_args()

  input_md = args.input
  output_path = args.output
  theme = args.theme
  themes_root = args.themes_root
  compiler = args.compiler

  if compiler is None:
    available_compilers = [ comp for comp in LATEX_COMPILERS if sh.which(comp) is not None ]
    if len(available_compilers) == 0:
      raise Exception(
        'Seems like there is no latex compiler on the system.'
      )
    else:
      compiler = available_compilers[0]
  else:
    if sh.which(compiler) is None:
      raise Exception(
        'Seems like %s is not installed' % (compiler, )
      )

  print('Using %s as LaTeX compiler' % (compiler, ))

  copy_source = args.source
  copy_parsed = args.parsed

  with open(input_md) as f:
    text = f.read()

  parsed = document()(text)

  ### Preparing env
  working_dir = os.path.join(tempfile.mkdtemp('mdslides'), 'mdslides')
  source_dir = os.path.dirname(os.path.abspath(input_md))

  try:
    print('Copying project files (%s) into working directory (%s)' % (source_dir, working_dir))
    sh.copytree(source_dir, working_dir)

    theme_dir, template_path = locate_theme(theme, source_dir, themes_root=themes_root)

    print('Transferring theme files into %s:' % working_dir)
    for item in os.listdir(theme_dir):
      path = os.path.join(theme_dir, item)
      print('  copying %s...' % path)
      if os.path.isfile(path):
        sh.copy(path, working_dir)
      else:
        sh.copytree(path, os.path.join(working_dir, item))

    with open(template_path) as f:
      template = f.read()

    generator = Generator(renderers[renderer], template=template, theme=theme, working_dir=working_dir)
    code = generator(parsed)

    source_name = os.path.basename(input_md)
    ftokens = source_name.split('.')
    if len(ftokens) > 1 and ftokens[-1] == 'md':
      fname = '.'.join(ftokens[:-1])
    else:
      fname = source_name

    code_file = os.path.join(working_dir, fname + '.tex')
    pdf_file = os.path.join(working_dir, fname + '.pdf')
    print('Writing compiled code into %s' % code_file)
    with open(code_file, 'w') as f:
      f.write(code)

    try:
      compiler = Compiler(latex=compiler)
      compiler.compile(code_file=code_file, working_dir=working_dir)
    finally:
      if copy_source:
        try:
          print('Copying result %s into %s' % (code_file, source_dir))
          sh.copy(code_file, source_dir)
        except Exception as e:
          print('Failed')
          print(e)

      if copy_parsed:
        try:
          parsed_file = os.path.join(source_dir, fname + '.parsed')
          print('Writing parsed source into %s' % (parsed_file,))
          with open(parsed_file, 'w') as f:
            f.write(parsed.pretty())
        except Exception as e:
          print('Failed')
          print(e)

      try:
        print('Copying result %s into %s' % (pdf_file, output_path))
        sh.copy(pdf_file, output_path)
      except:
        print('Failed')
        raise

  finally:
    print('Cleaning %s' % os.path.dirname(working_dir))
    sh.rmtree(os.path.dirname(working_dir))