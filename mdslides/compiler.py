import subprocess

class Compiler(object):
  def __init__(self, latex='xelatex', bibtex='bibtex'):
    self.latex = latex
    self.bibtex = bibtex

  def run(self, args):
    print('Invoking %s' % ' '.join(args))

    proc = subprocess.Popen(args, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

    stdout, stderror = proc.communicate()

    if proc.returncode != 0:
      print("STDOUT:\n%s" % stdout.decode('UTF-8'))
      print("STDERR:\n%s" % stderror.decode('UTF-8'))
      raise Exception('Compiler failed!')

  def run_latex(self, code_file, working_dir, escape=True):
    import os
    current_path = os.getcwd()

    print('Changing current directory to %s' % working_dir)
    os.chdir(working_dir)
    try:
      args = [
        self.latex,
        '-synctex=1', '-shell-escape' if escape else '', '-interaction=nonstopmode',
        code_file
      ]

      self.run(args)
    finally:
      print('Back to %s' % current_path)
      os.chdir(current_path)

  def run_bibtex(self, code_file, working_dir):
    import os
    current_path = os.getcwd()

    print('Changing current directory to %s' % working_dir)
    os.chdir(working_dir)
    try:
      args = [
        self.bibtex,
        '-shell-escape', '-synctex=1', '-interaction=nonstopmode',
        code_file
      ]

      self.run(args)
    finally:
      print('Back to %s' % current_path)
      os.chdir(current_path)

  def compile(self, code_file, working_dir):
    self.run_latex(code_file, working_dir)
    self.run_latex(code_file, working_dir)
