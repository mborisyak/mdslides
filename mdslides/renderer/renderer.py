from ..parser import *
from ..generator.utils import convert_arguments, split_by

def initialize_extension(ext, renderer, code, document, args):
  ext_kwargs = convert_arguments(args)

  try:
    return ext(renderer, code, document, **ext_kwargs)
  except TypeError as e:
    import inspect
    signature = ', '.join([
      (
        '%s' % (p.name,)
        if p.default == inspect.Parameter.empty
        else '%s=%s' % (p.name, p.default)
      ) for p in inspect.signature(ext).parameters.values()
    ])
    raise Exception(
      'Extension %s(%s) failed to initialize' % (ext.__name__, signature)
    ) from e


class Renderer(object):
  def __init__(self, extensions, working_dir):
    self.working_dir = working_dir

    self.extensions = [
      initialize_extension(ext, self, code, document, args)
      for ext, code, document, args in extensions
    ]

    self._render_methods = self._default_render_dict()

    for ext in self.extensions[::-1]:
      for attr in dir(ext):
        method = getattr(ext, attr)

        if not attr.startswith('render_') or not callable(method):
          continue

        name = attr[7:]
        if name in self._render_methods:
          print('Overriding %s with extension %s' % (attr, ext.__class__.__name__))

        self._render_methods[name] = method

  def packages(self):
    return [
      'hyperref',
      'amsmath',
      'amsfonts',
      'amssymb',
      ('ulem', 'normalem'),
    ] + [
      package
      for ext in self.extensions
      for package in ext.packages()
    ]

  @staticmethod
  def default_extensions():
    return ['footnotes', 'image', 'tikz', 'graphviz', 'minted']

  def _default_render_dict(self):
    return {
      attr[7:]: getattr(self, attr)
      for attr in dir(self)
      if attr.startswith('render_')
    }

  def __call__(self, title, sections):
    title = self.render(title)
    document = '\n'.join([
      self.render_section(header, section)
      for header, section in sections
    ])

    return title, document

  def render(self, obj):
    if obj is None:
      return ''

    clazz = type(obj).__name__
    try:
      renderer = self._render_methods[clazz]
    except KeyError:
      raise NotImplementedError('There is no method for rendering %s: %s' % (clazz, obj))

    return renderer(obj)

  def render_str(self, obj):
    return obj

  def render_list(self, obj):
    return ''.join([
      self.render(x)
      for x in obj
    ])

  def render_section(self, header : Header, obj):
    """
    If header is None then this method is called to render a slide outside any section.
    """
    if header is None or header.body is None:
      section = ''
    else:
      section = '\\section{%s}\n' % self.render(header.body)

    intersubsections, subsections = split_by(
      obj,
      sep_predicate=lambda x: isinstance(x, Header) and len(x.level.strip()) == 3
    )

    return '%s%s\n%s' % (
      section,
      self.render(intersubsections),
      '\n'.join([
        self.render_subsection(subsection_header, subsection)
        for subsection_header, subsection in subsections
      ])
    )

  def render_subsection(self, header : Header, obj):
    return "\\begin{frame}[fragile]\n\\frametitle{%s}\n%s\\end{frame}\n" % (
      self.render(header.body),
      self.render(obj)
    )

  def render_Header(self, obj):
    raise Exception('Headers are handled separately, this method should have not be called! %s' % (obj, ))

  def render_InlineMath(self, obj : InlineMath):
    return '$%s$' % (obj.value, )

  def render_Equation(self, obj : InlineMath):
    return '$$%s$$' % (obj.value, )

  def render_Block(self, obj : Block):
    if obj.func is None:
      return obj.body
    else:
      args = self.render(obj.args)
      args = '{%s}' % (args, ) if len(args) > 0 else ''

      opt_args = self.render(obj.opt_args)
      opt_args = '[%s]' % (opt_args,) if len(opt_args) > 0 else ''

      return '\\begin{%s}%s%s\n%s\\end{%s}\n' % (
        obj.func,
        opt_args,
        args,
        obj.body,
        obj.func,
      )

  def render_Code(self, obj : Code):
    if obj.func is None:
      return obj.body
    else:
      for ext in self.extensions:
        if ext.accepts_func(obj.func):
          return ext(obj.func, obj.args, obj.body)

      raise Exception('No extension to parse %s' % (obj.func, ))

  def render_FootnoteRef(self, obj : FootnoteRef):
    return ''

  def render_FootnoteDef(self, obj : FootnoteDef):
    return ''

  def render_FootnoteDefRef(self, obj : FootnoteDefRef):
    return '%s %s' % (
      self.render_FootnoteDef(obj),
      self.render_FootnoteRef(obj)
    )

  def render_HRule(self, obj : HRule):
    return ''

  def render_Image(self, obj : Image):
    raise Exception('images are handled by an extension. This method should have not been called.')

  def render_Link(self, obj : Link):
    text = '' if obj.text is None else self.render(obj.text)

    url = obj.ref.replace('#', '\\#')
    url = url.replace('%', '\\%')
    url = url.replace('&', '\\&')

    if len(text) == 0:
      return '\\url{%s}' % (url,)
    else:
      return '\\href{%s}{%s}' % (url, text)

  def render_Inline(self, obj : Inline):
    return '%s' % (obj.value, )

  def render_Starred(self, obj : Starred):
    return '\\textit{%s}' % (self.render(obj.value), )

  def render_DoublyStarred(self, obj : DoublyStarred):
    return '\\textbf{%s}' % (self.render(obj.value), )

  def render_DoublyCurly(self, obj : DoublyStarred):
    return '\\sout{%s}' % (self.render(obj.value), )

  def render_Underlined(self, obj : Underlined):
    return '\\texttt{%s}' % (self.render(obj.value), )

  def render_DoublyUnderlined(self, obj : DoublyUnderlined):
    return '\\underline{%s}' % (self.render(obj.value), )

  def render_RichText(self, obj : RichText):
    return ''.join([self.render(o) for o in obj])

  def render_Itemize(self, obj : Itemize):
    items = [self.render(x) for x in obj]
    if not items[-1].endswith('\n'):
      new_line = '\n'
    else:
      new_line = ''

    return '\\begin{itemize}\n  %s\n\\end{itemize}%s' % (
      '\n  '.join(items),
      new_line
    )

  def render_Enumerate(self, obj : Enumerate):
    items = [self.render(x) for x in obj]
    if not items[-1].endswith('\n'):
      new_line = '\n'
    else:
      new_line = ''

    return '\\begin{enumerate}\n  %s\n\\end{enumerate}%s' % (
      '\n  '.join(items),
      new_line
    )

  def render_Quote(self, obj : Quote):
    items = [self.render(x) for x in obj]

    if not items[-1].endswith('\n'):
      new_line = '\n'
    else:
      new_line = ''

    return '\\begin{quote}\n%s\n\\end{quote}%s' % (
      '\n  '.join(items),
      new_line
    )

  def render_Item(self, obj : Item):
    return '\\item %s' % (self.render(obj.value),)

  def render_Paragraph(self, obj : Paragraph):
    return self.render(obj.value)

  def render_ArgumentList(self, obj : ArgumentList):
    return ', '.join([
      self.render(x)
      for x in obj
    ])

  def render_int(self, obj : int):
    return str(obj)

  def render_float(self, obj : float):
    return str(obj)

  def render_String(self, obj : String):
    return '{%s}' % (obj.value, )

  def render_Argument(self, obj : Argument):
    if obj.value is None:
      return self.render(obj.key)
    else:
      return '%s=%s' % (
        obj.key,
        self.render(obj.value)
      )