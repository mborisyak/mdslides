from ..parser import Header, HRule
from .renderer import Renderer

def priority(obj : HRule):
  sym = obj.value[0]
  spacing = len(obj.value.split(sym)[1])

  return 2 * spacing + (1 if sym == '*' else 0)

def split_slide(objs):
  priorities = [
    priority(obj) for obj in objs
    if isinstance(obj, HRule)
  ]

  if len(priorities) == 0:
    return None, [objs]

  max_priority = max(priorities)

  groups = list()
  group = list()

  for obj in objs:
    if isinstance(obj, HRule) and priority(obj) == max_priority:
      groups.append(group)
      group = list()
    else:
      group.append(obj)

  groups.append(group)

  return (max_priority % 2 == 1), groups

class Beamer(Renderer):
  def packages(self):
    return [('hyphenat', 'none')] + super(Beamer, self).packages()

  def render_subsection(self, header : Header, obj):
    header = self.render(header.body)
    body = self.render_group(obj)
    if not body.endswith('\n'):
      body = '%s\n' % (body, )

    return "\\begin{frame}[fragile]\n\\frametitle{%s}\n%s\\end{frame}\n" % (
      header,
      body
    )

  def render_group(self, obj):
    vertical, groups = split_slide(obj)

    if len(groups) == 1:
      return ''.join([
        self.render(x) for x in obj
      ])

    rendered_groups = [
      self.render_group(group)
      for group in groups
    ]

    if vertical:
      width = 0.99 / len(groups)

      return '\\begin{columns}\n%s\\end{columns}\n' % (
        '\n'.join([
          '\\begin{column}{%.3lf\\textwidth}\n%s\\end{column}\n' % (width, group)
          for group in rendered_groups
        ]),
      )
    else:
      return '\n'.join(rendered_groups)

  def render_HRule(self, obj : HRule):
    return ''