from mdslides.parser import Header, HRule
from .renderer import Renderer


class LaTeX(Renderer):
  @staticmethod
  def default_extensions():
    return ['abstract',] + Renderer.default_extensions()

  def render_subsection(self, header : Header, obj):
    return "\\medskip\\noindent\n\\textbf{%s}\\hfill\\break\n%s" % (
      self.render(header.body),
      ''.join([self.render(x) for x in obj])
    )

  def render_HRule(self, obj : HRule):
    return ''