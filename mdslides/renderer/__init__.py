from .beamer_renderer import Beamer
from .latex_renderer import LaTeX

renderers = dict(
  beamer=Beamer,
  latex=LaTeX
)