import re
from ..parser import Token, TokenTuple

__all__ = [
  'filter_parse_tree',
  'replace_whitespaces'
]

_white_space_reg = re.compile(r'\s+')
def replace_whitespaces(string, sub='-'):
  return _white_space_reg.sub(sub, string)

def filter_parse_tree(obj, predicate):
  results = list()
  stack = [obj]

  while len(stack) > 0:
    node = stack.pop()

    if predicate(node):
      results.append(node)
    else:
      if isinstance(node, Token):
        value = node.value

        if isinstance(value, (list, )):
          stack.extend(value)
        else:
          stack.append(value)
      elif isinstance(node, (TokenTuple, )):
        stack.extend(node)
      elif isinstance(node, (list, tuple)):
        stack.extend(node)

  return results