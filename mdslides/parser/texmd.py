from .grammar import *
from .utils import *

__all__ = [
  'String', 'Argument', 'ArgumentList',
  'Inline', 'DoublyStarred', 'Starred', 'Underlined', 'DoublyUnderlined',
  'InlineMath', 'Equation',
  'Image', 'Link', 'FootnoteRef', 'FootnoteDef', 'FootnoteDefRef',
  'RichText',
  'Item', 'Enumerate', 'Itemize', 'Quote',
  'Block', 'Code',
  'HRule', 'Header',
  'Paragraph', 'Document',
  
  'real', 'integer', 'word', 'latex_string', 'space',
  'string', 'value', 'flag', 'key_value',
  'argument', 'arguments', 'argument_list',
  'inline', 'doubly_underlined', 'underlined', 'doubly_starred', 'starred',
  'inline_math', 'equation',
  'image', 'link', 'footnote_ref', 'footnote_def', 'footnote_defref', 'link_footnote',
  'rich_text',
  'dash_item', 'plus_item', 'numbered_item', 'enumerated', 'itemized', 'quote_item', 'quote',
  'block', 'code',
  'hrule', 'header',
  'paragraph', 'document',
]

@pattern
def real():
  return reg(float)('[+-]?\\d+[.]\\d+')

@pattern
def integer():
  return reg(int)('[+-]?\\d+')

@pattern
def number():
  return either()(real, integer)

@pattern
def word():
  return reg(str)('[A-Za-z_]\\w*')

@pattern
def latex_string():
  return reg(str)('[\\w.\\\\]+')

@pattern
def space():
  return reg()('[ \\t]*')

String = token('String', ['value', 'modifier'], defaults=[None, None])

@pattern
def string():
  return either()(
    isolated_string(String)('\'', allow_empty=True, allow_new_line=False, modifiers=['r'])(
      value=unescaped(escape_symbols=["'"], special_symbols=True),
      modifier=identity
    ),
    isolated_string(String)('\"', allow_empty=True, allow_new_line=False, modifiers=['r'])(
      value=unescaped(special_symbols=True),
      modifier=identity
    ),
  )

@pattern
def value():
  return either()(number, string, latex_string)

Argument = token('Argument', ['key', 'value'], defaults=[None,])

@pattern
def flag():
  return seq(Argument)(key=either()(string, latex_string))

@pattern
def key_value():
  return seq(Argument)(key=word, _sep='[ ]*=[ ]*', value=value)

@pattern
def argument():
  return either()(key_value, flag)

@pattern
def arguments():
  return repeat()(argument, sep='[ \\t]*,[ \\t]*')

ArgumentList = token('ArgumentList')
@pattern
def argument_list():
  return seq(ArgumentList, strict=True)(
    _opening='\\[',
    _spacing1=space,
    value=arguments(),
    _spacing2=space,
    _ending='\\]'
  )

Inline = token('Inline')
@pattern
def inline():
  return isolated_string(Inline)('`', allow_empty=False, allow_new_line=True, escaped=False)(
    value=str
  )

InlineMath = token('InlineMath')
@pattern
def inline_math():
  return isolated_string(InlineMath)('$', allow_empty=False, allow_new_line=False, escaped=False)(
    value=str
  )

DoublyStarred = token('DoublyStarred')
@pattern
def doubly_starred():
  return isolated_string(DoublyStarred)('**', allow_empty=False, allow_new_line=True)(
    value=unescaped(f=inlines(), escape_symbols='*', special_symbols=False)
  )

DoublyCurly = token('DoublyCurly')
@pattern
def doubly_curly():
  return isolated_string(DoublyCurly)('~~', allow_empty=False, allow_new_line=True)(
    value=unescaped(f=inlines(), escape_symbols='~', special_symbols=False)
  )

Starred = token('Starred')
@pattern
def starred():
  return isolated_string(Starred)('*', allow_empty=False, allow_new_line=True)(
    value=unescaped(f=inlines(), escape_symbols='*', special_symbols=False)
  )

Underlined = token('Underlined')
@pattern
def underlined():
  return isolated_string(Underlined)('_', allow_empty=False, allow_new_line=True)(
    value=unescaped(f=inlines(), escape_symbols='_', special_symbols=False)
  )

DoublyUnderlined = token('DoublyUnderlined')
@pattern
def doubly_underlined():
  return isolated_string(DoublyUnderlined)('__', allow_empty=False, allow_new_line=True)(
    value=unescaped(f=inlines(), escape_symbols='_', special_symbols=False)
  )

Image = token('Image', ['arguments', 'ref'])
@pattern
def image():
  return seq(Image, strict=True)(
    arguments=seq(ArgumentList, strict=False)(
      _opening='!\\[',
      _spacing1 = space,
      value = arguments,
      _spacing2=space,
      _ending='\\]'
    ),
    _ref_opening='\\(',
    ref='[^)\\n]+',
    _ref_ending='\\)'
  )

Link = token('Link', ['text', 'ref'])
@pattern
def link():
  return seq(Link, strict=False)(
    _opening='(?<![!])\\[',
    text=reg(rich_text())('[^\\]]*'),
    _ending='\\]',
    _ref_opening='\\(',
    ref='[^)\\n]+',
    _ref_ending='\\)'
  )

FootnoteDef = token('FootnoteDef', ['ref', 'text'])
@pattern
def footnote_def():
  return seq(FootnoteDef, strict=False)(
    _opening='(?<![!])\\n[ \t]*\\[',
    ref=reg(unnew_line)('[\\s\\w\\d_\\-:]+'),
    _ending='\\]: ',
    _spacing=space,
    text=reg(rich_text())('[^\\n]+'),
  )

FootnoteRef = token('FootnoteRef', ['ref'])
@pattern
def footnote_ref():
  return seq(FootnoteRef, strict=False)(
    _opening='(?<![!\\n])\\[',
    ref=reg(unnew_line)('[\\s\\w\\d_\\-:]+'),
    _ending='\\](?![\\[(])',
  )

FootnoteDefRef = token('FootnoteDefRef', ['ref', 'text'])
@pattern
def footnote_defref():
  return seq(FootnoteDefRef, strict=False)(
    _opening='(?<![!])\\[',
    ref=reg(rich_text())('[^\\]]*'),
    _ending='\\]',
    _ref_opening='\\[',
    text='[^]\\n]+',
    _ref_ending='\\]'
  )

@pattern
def link_footnote():
  return either()(
    footnote_def,
    footnote_ref,
    footnote_defref,
    link
  )

RichText = token('RichText')
@pattern
def inlines():
  return captured(RichText)()(
    either()(footnote_ref, link),
    inline,
    doubly_starred,
    starred,
    doubly_underlined,
    doubly_curly,
    underlined,
  )

@pattern
def rich_text():
  return captured(RichText)()(
    image,
    link_footnote,
    inline,
    inline_math,
    doubly_starred,
    starred,
    doubly_underlined,
    doubly_curly,
    underlined,
  )

Item = token('Item')
@pattern
def dash_item():
  return objreg(Item)(
    '(?<=\\n)(?P<level>[ ]*)[-]\\s+(?P<value>(.|[\\n])+?)($|\\n(?=\\n|(?P=level)([+]|[-]|\\d+[.])))',
    condition ='(?<=\\n)[ ]*[-][ ]'
  )(
    value=paragraph()
  )

@pattern
def plus_item():
  return objreg(Item)(
    '(?<=\\n)(?P<level>[ ]*)[+]\\s+(?P<value>(.|[\\n])+?)($|\\n(?=\\n|(?P=level)([+]|[-]|\\d+[.])))',
    condition ='(?<=\\n)[ ]*[+][ ]'
  )(
    value=paragraph()
  )

Itemize = token('Itemize')
@pattern
def itemized():
  return either()(
    repeat(Itemize)(dash_item),
    repeat(Itemize)(plus_item),
  )

@pattern
def numbered_item():
  return objreg(Item)(
    '(?<=\\n)(?P<level>[ ]*)\\d+[.]\\s+(?P<value>(.|[\\n])+?)($|\\n(?=\\n|(?P=level)([+]|[-]|\\d+[.])))',
    condition='(?<=\\n)[ ]*\\d+[.][ ]'
  )(
    value=paragraph()
  )

Enumerate = token('Enumerate')
@pattern
def enumerated():
  return repeat(Enumerate)(numbered_item)

@pattern
def quote_item():
  return objreg(identity)(
    '(?<=\\n)(?P<level>[ ]*[>])\\s+(?P<value>.+?)($|\\n)',
    condition ='(?<=\\n)[ ]*[>][ ]'
  )(
    value=rich_text()
  )

Quote = token('Quote')
@pattern
def quote():
  return repeat(Quote)(quote_item)

Block = token(
  'Block',
  ['func', 'args', 'opt_args', 'body']
)

@pattern
def latex_word():
  return reg(str)(r'[A-Za-z_][\w*]*')

@pattern
def block():
  return seq(Block, strict=True)(
    _opening='(?<=\\n)[ \\t]*~~~',
    func=optional()(latex_word),
    _spacing1=space,
    args=optional()(argument_list),
    _spacing2=space,
    opt_args=optional()(argument_list),
    _spacing3=space,
    _new_line='\\n',
    body=reg(unescaped(escape_symbols=['~'], special_symbols=False))(
      '(?:.|[\\n])*?(?<=\\n)(?=[ \\t]*~~~)'
    ),
    _ending='[ \\t]*~~~[ \\t]*(\\n|$)'
  )

Code = token(
  'Code',
  ['func', 'args', 'body']
)

@pattern
def code():
  return seq(Code, strict=True)(
    _opening='(?<=\\n)[ \\t]*```',
    func=optional()(word),
    _spacing1=space,
    args=optional()(argument_list),
    _spacing2=space,
    _new_line='\\n',
    body=reg(unescaped(escape_symbols=['`'], special_symbols=False))(
      '(?:.|[\\n])*?(?<=\\n)(?=[ \\t]*```)'
    ),
    _ending='[ \\t]*```[ \\t]*(\\n|$)'
  )

HRule = token('HRule')

def spaced(sym, repetition=3):
  import re

  sym = re.escape(sym)

  if repetition == 1:
    return sym
  else:
    return '(?:[ ]*)'.join([sym] * repetition)

@pattern
def hrule():
  return objreg(HRule)(
    '(?<=\\n)\\s*(?P<value>{horizontal}|{vertical})\\s*\\n'.format(
      horizontal=spaced('-'),
      vertical=spaced('*'),
    )
  )(
    value=str
  )

DoublyNewLine = token('DoublyNewLine')
@pattern
def doubly_new_line():
  return reg(DoublyNewLine)('\\n\\n')

Equation = token('Equation')
@pattern
def equation():
  return isolated_string(Equation)('$$', allow_empty=False, allow_new_line=False, escaped=False)(
    value=str
  )

Paragraph = token('Paragraph')
@pattern
def paragraph():
  return captured(Paragraph)(rich_text())(
    equation,
    doubly_new_line,
    block,
    code,
    hrule,
    itemized,
    enumerated,
  )

Header = token('Header', ['level', 'body'])
@pattern
def header():
  return seq(Header)(level='(?<![^\\n])\\#{1,5}', _space='[ \t]*', body=reg(rich_text())('.*'), _new_line='\\n(\\n)?')

Document = token('Document')

@pattern
def document():
  return captured(Document)(rich_text())(
    header,

    block,
    code,
    hrule,
    itemized,
    enumerated,
    quote
  )