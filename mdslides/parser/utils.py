import re
from collections import namedtuple

__all__ = [
  'unescaped',
  'unnew_line',
  'was_expected',
  'identity',
  'pattern',
  'Token', 'TokenTuple', 'token',
]

def identity(value):
  return value

def pattern(f):
  def __init__(self, f):
    self.value = None
    self.f = f

  def __call__(self, ):
    if self.value is None:
      self.value = self.f()

    return self.value

  clazz = type(
    f.__name__,
    (object, ),
    dict(
      __init__=__init__,
      __call__=__call__
    )
  )

  instance = clazz(f)
  return instance

def indent(xs, level):
  lines = xs.split('\n')
  sep = '\n' + '  ' * (level + 1)
  return sep.join(lines)

def format(x, level):
  if isinstance(x, str):
    return indent(x, level=level)

  elif hasattr(x, 'pretty'):
    return x.pretty(level=level)

  else:
    try:
      ind = '  ' * level
      sep = '\n' + ind
      return sep.join([
        format(item, level=level)
        for item in x
      ])

    except (TypeError, AttributeError):
      return indent(str(x), level=level)

class Token(object):
  def __init__(self, value):
    self.value = value

  def pretty(self, level=0):
    indent = '  ' * level
    sep = '\n  ' + indent

    if isinstance(self.value, list):
      formatted = sep.join([ format(x, level=level + 1) for x in self.value ])
      return '%s([\n%s  %s\n%s])' % (
        self.__class__.__name__,
        indent,
        formatted,
        indent
      )
    else:
      formatted = format(self.value, level=level + 1)
      return '%s(\n%s  %s\n%s)' % (
        self.__class__.__name__,
        indent,
        formatted,
        indent
      )

  def __call__(self):
    return self.value

  def __iter__(self):
    return iter(self.value)

  def __getitem__(self, item):
    return self.value[item]

  def __str__(self, ):
    return '%s(%s)' % (self.__class__.__name__, str(self.value))

  def __repr__(self, ):
    return '%s(%s)' % (self.__class__.__name__, repr(self.value))

class TokenTuple(object):
  def pretty(self, level=0):
    indent = '  ' * level

    formatted = '\n'.join([
      '%s  %s=%s' % (indent, field, format(getattr(self, field), level=level+1))
      for field in self._fields
    ])
    return '%s(\n%s\n%s)' % (
      self.__class__.__name__,
      formatted,
      indent
    )

def token(name, fields=None, defaults=None):
  if fields is None:
    return type(name, (Token, ), dict())
  else:
    original = namedtuple(name, fields, defaults=defaults)
    return type(name, (original, TokenTuple), dict())

def _unescape_special(match):
  symbol = match.group(1)
  if symbol == 'n':
    return '\n'
  elif symbol == 't':
    return '\t'
  else:
    return symbol

class unescaped(object):
  def __init__(self, f=identity, escape_symbols=None, special_symbols=False):
    self.f = f

    if special_symbols and escape_symbols is not None:
      escape_symbols = list(escape_symbols) + ['t', 'n']

    if escape_symbols is None:
      self.unescape_reg = re.compile('\\\\(.)')
    else:
      self.unescape_reg = re.compile(
        '\\\\(%s)' % ('|'.join([re.escape(x) for x in escape_symbols]),)
      )

    if special_symbols:
      self._unescape = '\\g<1>'
    else:
      self._unescape = _unescape_special

  def __call__(self, value):
    return self.f(
      self.unescape_reg.sub(self._unescape, value)
    )

new_line_reg = re.compile('[\\n]')
def unnew_line(value):
  return new_line_reg.sub(' ', value)

def format_string(string, pos=0, endpos=None, span=25):
  if endpos is None:
    endpos = len(string)

  print(pos, span)
  start = pos - span
  end = pos + span
  if end > endpos:
    start -= end - endpos
    end = endpos

  if start < 0:
    start = 0

  left = string[start:pos]
  right = string[pos:end]

  return '%s|%s' % (left, right)

def was_expected(seq, pattern, string, pattern_start, pos, results, span=20):
  start = max(pos - span, 0)
  context_before = string[start:pos]

  indent = lambda x: '\n'.join([ '  %s' % (line, ) for line in x.split('\n') ])

  context = '\n'.join([
    '%s={\n%s\n}' % (k, indent(repr(v)))
    for k, v in results.items()
  ])

  end = min(pos + span, len(string))
  context_after = string[pos:end]

  return SyntaxError('Expecting:\n%s\n  from\n%s\n  in\n%s\n%s\n%s' % (
    pattern, seq, context_before, context, context_after
  ))