import re
from .utils import *

__all__ = [
  'Pattern',
  'to_pattern',
  'to_patterns',

  'RegExp', 'reg',
  'ObjectReg', 'objreg',
  'Either', 'either',
  'Optional', 'optional',
  'Sequence', 'seq',
  'isolated_string',
  'Repeat', 'repeat',

  'Captured', 'captured'
]

class Pattern(object):
  def __call__(self, string, pos=0, endpos=None):
    _, _, value = self.check(string, pos, endpos)
    return value

  def match(self, string, pos=0, endpos=None):
    start, end, value = self.check(string, pos, endpos)
    if start is None:
      raise was_expected(self, self, string, pos, pos, results=dict())

    return start, end, value

  def check(self, string, pos=0, endpos=None):
    raise NotImplementedError()

  def search(self, string, pos=0, endpos=None):
    raise NotImplementedError()

  def condition(self):
    raise NotImplementedError()

def to_pattern(pat):
  if isinstance(pat, Pattern):
    return lambda : pat

  if callable(pat):
    return pat

  elif isinstance(pat, str):
    value = RegExp(pat, str)
    return lambda : value

  elif isinstance(pat, tuple):
    kwargs = {
      'item_%s' % (i, ) : to_pattern(p)
      for i, p in enumerate(pat)
    }
    from collections import namedtuple

    t = namedtuple('obj', [ k for k in kwargs if not k.startswith('_') ])
    pat = seq(t, True)(**kwargs)
    return lambda : pat

  elif isinstance(pat, list):
    value = either(*pat)
    return lambda : value

  else:
    raise ValueError('pattern %s is not understood' % (pat,))

def to_patterns(patterns):
  if isinstance(patterns, dict):
    return {
      k : to_pattern(patterns[k])
      for k in patterns
    }
  elif isinstance(patterns, (tuple, list)):
    return [
      to_pattern(pattern)
      for pattern in patterns
    ]
  else:
    raise Exception('patterns %s are not understood' % (patterns, ))

class RegExp(Pattern):
  def __init__(self, expr, f=str):
    self.r = re.compile(expr)
    self.f = f

  def convert(self, match):
    if match is None:
      return None, None, None
    else:
      start, end = match.span()
      return start, end, self.f(match.group(0))

  def check(self, string, pos=0, endpos=None):
    if endpos is None:
      endpos = len(string)

    return self.convert(
      self.r.match(string, pos, endpos)
    )

  def search(self, string, pos=0, endpos=None):
    if endpos is None:
      endpos = len(string)

    return self.convert(
      self.r.search(string, pos, endpos)
    )

  def condition(self):
    return self.r.pattern

  def __str__(self):
    try:
      return '%s(%s)' % (self.f.__name__, self.r.pattern)
    except AttributeError:
      return self.r.pattern

def reg(f=identity):
  def p(expr):
    return RegExp(expr, f)

  return p

class ObjectReg(Pattern):
  def __init__(self, expr, f, condition=None, **kwargs):
    self.r = re.compile(expr)
    self.f = f

    self.kwargs = kwargs
    self._condition = condition

  def convert(self, match):
    if match is None:
      return None, None, None
    else:
      kwargs = dict()
      for k in self.kwargs:
        kwargs[k] = self.kwargs[k](match.group(k))

      start, end = match.span()
      return start, end, self.f(**kwargs)

  def check(self, string, pos=0, endpos=None):
    if endpos is None:
      endpos = len(string)

    return self.convert(
      self.r.match(string, pos, endpos)
    )

  def search(self, string, pos=0, endpos=None):
    if endpos is None:
      endpos = len(string)

    return self.convert(
      self.r.search(string, pos, endpos)
    )

  def condition(self):
    if self._condition is None:
      return self.r.pattern
    else:
      return self._condition

  def __str__(self):
    try:
      return self.f.__name__
    except AttributeError:
      return self.r.pattern

def objreg(f=dict):
  def m(expr, condition=None):
    def p(**kwargs):
      return ObjectReg(expr, f, condition, **kwargs)
    return p

  return m

class Either(Pattern):
  def __init__(self, f, *patterns):
    self.patterns = to_patterns(patterns)
    self.f = f

  def check(self, string, pos=0, endpos=None):
    for f in self.patterns:
      pat = f()
      start, end, match = pat.check(string, pos, endpos)

      if start is not None:
        return start, end, self.f(match)

    return None, None, None

  def search(self, string, pos=0, endpos=None):
    for f in self.patterns:
      pat = f()
      start, end, match = pat.search(string, pos, endpos)

      if start is not None:
        return start, end, self.f(match)

    return None, None, None

  def condition(self):
    return '(?:%s)' % (
      '|'.join([x().condition() for x in self.patterns]),
    )

  def __str__(self):
    return ' | '.join([
      str(x) for x in self.patterns
    ])

def either(f=identity):
  def p(*patterns):
    return Either(f, *patterns)
  return p

class Optional(Pattern):
  def __init__(self, f, pattern):
    self.pattern = to_pattern(pattern)
    self.f = f

  def check(self, string, pos=0, endpos=None):
    start, end, value = self.pattern().check(string, pos, endpos)
    if start is None:
      return pos, pos, None
    else:
      return start, end, self.f(value)

  def search(self, string, pos=0, endpos=None):
    start, end, value = self.pattern().search(string, pos, endpos)
    if start is None:
      return None, None, None
    else:
      return start, end, self.f(value)

  def condition(self):
    return '(?:%s)?' % (self.pattern().condition(), )

  def __str__(self):
    return '(%s)?' % (self.pattern(), )

def optional(f=identity):
  def p(pattern):
    return Optional(f, pattern)
  return p


class Sequence(Pattern):
  def __init__(self, f, strict, **kwargs):
    assert len(kwargs) > 0
    self.f = f
    self.kwargs = to_patterns(kwargs)

    keys = list(self.kwargs.keys())
    self.first_key, self.rest_keys = keys[0], keys[1:]

    self.strict = strict

  def _check_rest(self, string, pos, endpos, results):
    end = pos

    for k in self.rest_keys:
      pat = self.kwargs[k]()
      next_start, next_end, value = pat.check(string, end, endpos)

      if next_start is None:
        if self.strict:
          raise was_expected(self, pat, string, pos, end, results)
        else:
          return None, None, None
      else:
        end = next_end

      if not k.startswith('_'):
        results[k] = value

    return pos, end, self.f(**results)

  def condition(self):
    return self.kwargs[self.first_key]().condition()

  def check(self, string, pos=0, endpos=None):
    results = dict()

    first_pattern = self.kwargs[self.first_key]()
    start, end, value = first_pattern.check(string, pos, endpos)
    if start is None:
      return None, None, None

    if not self.first_key.startswith('_'):
      results[self.first_key] = value

    rest_start, rest_end, results = self._check_rest(string, end, endpos, results)
    if rest_start is None:
      return None, None, None
    else:
      return start, rest_end, results

  def search(self, string, pos=0, endpos=None):
    first_pattern = self.kwargs[self.first_key]()

    pattern_start, first_end, value = first_pattern.search(string, pos, endpos)
    results = dict() if self.first_key.startswith('_') else { self.first_key : value }

    while pattern_start is not None:
      rest_start, rest_end, results = self._check_rest(string, first_end, endpos, results=results)

      if rest_start is not None:
        return pattern_start, rest_end, results

      pattern_start, rest_start, value = first_pattern.search(string, pattern_start + 1, endpos)
      results = dict() if self.first_key.startswith('_') else { self.first_key : value }

    return None, None, None

  def __str__(self):
    try:
      name = self.f.__name__
    except AttributeError:
      name=''

    return '%s(%s)' % (
      name,
      ', '.join([
        '%s=%s' % (k, v())
        for k, v in self.kwargs.items()
      ])
    )

def seq(f=dict, strict=False):
  def p(**kwargs):
    return Sequence(f, strict, **kwargs)
  return p

class Repeat(Pattern):
  def __init__(self, f, pattern, sep=None, at_least=1):
    self.pattern = to_pattern(pattern)

    if sep is not None:
      self.sep = to_pattern(sep)
    else:
      self.sep = None

    self.at_least = at_least

    self.f = f

  def _check(self, string, pos, endpos, results):
    seq_end = pos
    start, end, match = self.pattern().check(string, pos, endpos)
    while end is not None:
      results.append(match)
      seq_end = end

      if self.sep is not None:
        start, end, match = self.sep().check(string, end, endpos)
        if end is None:
          break

      start, end, match = self.pattern().check(string, end, endpos)

    return pos, seq_end, results

  def check(self, string, pos=0, endpos=None):
    start, end, results = self._check(string, pos, endpos, [])

    if len(results) >= self.at_least:
      return start, end, self.f(results)
    else:
      return None, None, None

  def search(self, string, pos=0, endpos=None):
    results = list()

    start, end, match = self.pattern().search(string, pos, endpos)
    if start is None:
      return None, None, None

    results.append(match)
    _, end, results = self._check(string, end, endpos, results)

    if len(results) >= self.at_least:
      return start, end, self.f(results)
    else:
      return None, None, None

  def condition(self):
    return self.pattern().condition()

  def __str__(self):
    if self.at_least == 0:
      return '(%s)*' % (self.pattern, )
    elif self.at_least == 1:
      return '(%s)+' % (self.pattern, )
    else:
      return '(%s){%d, }' % (self.pattern, self.at_least)

def repeat(f=identity):
  def p(pattern, sep=None, at_least=1):
    return Repeat(f, pattern, sep=sep, at_least=at_least)
  return p

def isolated_string(f=identity):
  def p(symbol, allow_empty=False, allow_new_line=False, modifiers=None, escaped=True):
    import re
    symbol_re = re.escape(symbol)

    dot = '(?:\\n[^\\n]|.)' if allow_new_line else '.'
    mod = '*?' if allow_empty else '+?'

    if modifiers is None:
      modifiers_re = ''
    else:
      modifiers_re = '(?P<modifier>%s)?' % (
        '|'.join(modifiers)
      )

    if escaped:
      ending = '((?<!\\\\)%s)' % (symbol_re, )
    else:
      ending = '(%s)' % (symbol_re,)

    expr = '(?<!\\\\){modifiers}({symbol})(?P<value>{dot}{mod}){ending}'.format(
      modifiers=modifiers_re,
      symbol=symbol_re,
      dot=dot,
      mod=mod,
      ending=ending
    )

    return objreg(f)(expr, condition=symbol_re)
  return p

def _enrich(default, string, pos, endpos, patterns):
  if len(patterns) == 0 and pos < endpos:
    return [default(string[pos:endpos])]

  pattern = patterns[0]()
  results = list()

  current_pos = pos
  start, end, value = pattern.search(string, current_pos, endpos=endpos)
  while start is not None:
    if start == current_pos:
      results.append(value)
    elif start > current_pos:
      results.extend(
        _enrich(default, string, current_pos, start, patterns[1:])
      )
      results.append(value)
    else:
      raise Exception('Pattern does not respect starting position.')

    current_pos = end
    if current_pos == endpos:
      return results

    start, end, value = pattern.search(string, current_pos, endpos=endpos)

  if current_pos < endpos:
    results.extend(
      _enrich(default, string, current_pos, endpos, patterns[1:])
    )

  return results

class Captured(Pattern):
  def __init__(self, f, default, additives):
    self.default = default

    self.additives = {
      'g%d' % (i,): to_pattern(additive)
      for i, additive in enumerate(additives)
    }

    self.f = f
    self._search_re = None

  def search_re(self):
    if self._search_re is None:
      assert all([
        hasattr(additive(), 'condition')
        for additive in self.additives.values()
      ])

      search_pattern = '|'.join([
        '(?P<g%d>%s)' % (i, additive().condition())
        for i, additive in enumerate(self.additives.values())
      ])
      try:
        self._search_re = re.compile(search_pattern)
      except Exception as e:
        print(search_pattern)
        raise Exception('%s' % (search_pattern, )) from e

      return self._search_re
    else:
      return self._search_re

  def _capture(self, string, pos=0, endpos=None):
    if endpos is None:
      endpos = len(string)

    results = list()

    start = pos
    cursor = pos

    match = self.search_re().search(string, cursor, endpos)
    while match is not None:
      match_start, match_end = match.span()
      pattern = self.additives[match.lastgroup]

      if match.group(0).startswith('+'):
        print()

      pattern_start, pattern_end, value = pattern().check(string, pos=match_start, endpos=endpos)

      if pattern_start is not None:
        if pattern_start > start:
          residual = string[start:pattern_start]
          results.append(self.default(residual))
        results.append(value)

        start = pattern_end
        cursor = pattern_end
      else:
        cursor = cursor + 1

      match = self.search_re().search(string, cursor, endpos)

    if start < endpos:
      results.append(
        self.default(string[start:endpos])
      )

    return pos, endpos, self.f(results)

  def condition(self):
    return '|'.join([
      '%s' % additive().condition()
      for additive in self.additives.values()
    ])

  def check(self, string, pos=0, endpos=None):
    return self._capture(string, pos=pos, endpos=endpos)

  def search(self, string, pos=0, endpos=None):
    return self._capture(string, pos=pos, endpos=endpos)

def captured(f=identity):
  def m(default=identity):
    def p(*additives):
      return Captured(f, default, additives)
    return p

  return m