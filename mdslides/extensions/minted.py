from .extension import CodeExtension

class MintedExtension(CodeExtension):
  def __init__(self, renderer, code=None, document=None, **kwargs):
    self.kwargs = kwargs
    super(MintedExtension, self).__init__(renderer, code)

  def packages(self):
    return ['minted']

  def environment(self):
    config = '%s\n%s' % (
      '\n'.join([
        '\\setminted{%s=%s}' % (k, v)
          for k, v in self.kwargs.items()
      ]),
      self.code
    )


    return dict(preamble=config)

  def accepts_func(self, func):
    return True

  def __call__(self, func, args, body):
    from ..generator.utils import pop_argument
    if func == 'minted':
      lang, args = pop_argument(args, 'lang')
    else:
      lang = func

    if lang is None:
      lang = 'md'

    return '\\begin{minted}[%s]{%s}\n%s\\end{minted}\n' % (
      self.renderer.render(args) if args is not None else '',
      lang,
      body,
    )