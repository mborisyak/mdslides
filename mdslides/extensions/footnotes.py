from ..parser import FootnoteRef, FootnoteDef, FootnoteDefRef
from ..renderer.utils import replace_whitespaces

from .extension import CodeExtension

__all__ = [
  'FootnoteExtension',
  'collect_footnotes'
]

def collect_footnotes(document):
  from ..renderer.utils import filter_parse_tree
  return filter_parse_tree(
    document,
    lambda x: isinstance(x, (FootnoteDef, FootnoteDefRef))
  )

class FootnoteExtension(CodeExtension):
  def __init__(self, renderer, code, document):
    super(FootnoteExtension, self).__init__(renderer, code, document)

    self.references = dict()
    for obj in collect_footnotes(document):
      label = replace_whitespaces(obj.ref, )
      self.references[label] = obj.text

  def packages(self):
    return []

  def accepts_func(self, func):
    return False

  def render_FootnoteRef(self, obj : FootnoteRef):
    label = replace_whitespaces(obj.ref, )
    if label not in self.references:
      import warnings
      warnings.warn('Unknown reference [%s]' % (obj.ref, ))
      return '[%s]' % (obj.ref, )

    text = self.references[label]
    return '\\footnote{%s}' % (
      self.renderer.render(text),
    )

  def render_FootnoteDef(self, obj : FootnoteDef):
    return ''

  def render_FootnoteDefRef(self, obj : FootnoteDefRef):
    return self.render_FootnoteRef(obj)

