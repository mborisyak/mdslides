from ..parser import FootnoteRef, FootnoteDef, Header
from ..parser import Enumerate, Item
from ..renderer.utils import replace_whitespaces

from .extension import CodeExtension

__all__ = [
  'ReferenceExtension',
]

class ReferenceExtension(CodeExtension):
  def __init__(self, renderer, code, document=None, bibliography=False, style='[%d]', afterword=True, title='References'):
    self.bibliography = True if bibliography != False else False
    self.style = style
    self.afterword = afterword
    self.title = title

    self.references_counters = dict()
    self.references = dict()
    self.counter = 1

    super(ReferenceExtension, self).__init__(renderer, code)

  def packages(self):
    if self.bibliography:
      return []
    else:
      return ['hyperref']

  def environment(self):
    if self.afterword and len(self.references) > 0:
      return dict(afterword=self(None, None, None))
    else:
      return dict()

  def accepts_func(self, func):
    return func == 'references'

  def render_FootnoteRef(self, obj : FootnoteRef):
    label = replace_whitespaces(obj.ref, )
    if label not in self.references_counters:
      self.references_counters[label] = self.counter
      self.counter += 1

    if self.bibliography:
      return '\\cite{%s}' % (label, )
    else:
      return '\\hyperlink{ref:%s}{%s}' % (
        label, self.style % (self.references_counters[label], )
      )

  def render_FootnoteDef(self, obj : FootnoteDef):
    label = replace_whitespaces(obj.ref)
    if label not in self.references_counters:
      self.references_counters[label] = self.counter
      self.counter += 1

    self.references[label] = '\\hypertarget{ref:%s}{%s}' % (
      label,
      self.renderer.render(obj.text)
    )

    return ''

  def __call__(self, func, args, body):
    if self.bibliography:
      return '\\begin{thebibliography}{%d}\n%s\n\\end{thebibliography}' % (
        len(self.references),
        '\n'.join([
          '\\bibitem{%s} %s' % (k, self.renderer.render(self.references[k]))
          for k in self.references
        ])
      )
    else:
      return self.renderer.render_section(
        Header(level='##', body=self.renderer.render(self.title)),
        Enumerate([
          Item(self.references[label])
          for label in self.references
        ])
      )

