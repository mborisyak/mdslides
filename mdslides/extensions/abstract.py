from .extension import CodeExtension

class AbstractExtension(CodeExtension):
  def __init__(self, renderer, code=None, document=None):
    super(AbstractExtension, self).__init__(renderer, code)

  def environment(self):
    if self.code is not None and len(self.code) > 0:
      abstract = '\\begin{abstract}\n%s\\end{abstract}' % (
        self.renderer.render(self.code),
      )
      return dict(
        preface=abstract
      )
    else:
      return dict()

  def packages(self):
    return []

  def accepts_func(self, func):
    return False

  def __call__(self, func, args, body):
    raise NotImplementedError()