from .abstract import AbstractExtension
from .graphviz import GraphvizExtension
from .image import ImageExtension
from .reference import ReferenceExtension
from .minted import MintedExtension
from .tikz import TikzExtension
from .footnotes import FootnoteExtension
from .info import InfoExtension

available_extensions = dict(
  info=InfoExtension,
  abstract=AbstractExtension,
  graphviz=GraphvizExtension,
  image=ImageExtension,
  references=ReferenceExtension,
  tikz=TikzExtension,
  minted=MintedExtension,
  footnotes=FootnoteExtension
)
