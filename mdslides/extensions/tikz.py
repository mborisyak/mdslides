from .extension import CodeExtension
from ..parser import Argument


class TikzExtension(CodeExtension):
  def packages(self):
    return ['tikz']

  def accepts_func(self, lang):
    return lang == 'tikz'

  def __call__(self, func, args, body):
    from ..generator.utils import render_arguments, pop_argument
    if args is not None:
      nofigure, args = pop_argument(args, 'nofigure', True, False)
      if not nofigure:
        caption, args = pop_argument(args, 'caption', None, None)
      else:
        caption = None

      tikz_args = '[%s]' % (render_arguments(args), )
    else:
      nofigure = False
      tikz_args = ''
      caption = None

    body = \
      '\\begin{tikzpicture}%s\n' \
      '%s\n' \
      '\\end{tikzpicture}\n' % (tikz_args, body)

    if nofigure:
      return body
    else:
      if caption is None:
        caption = ''
      else:
        caption = '\\caption{%s}' % (self.renderer.render(caption), )
      return \
        '\\begin{figure}[!h]\n' \
        '\\centering\n' \
        '%s\n' \
        '%s\n' \
        '\\end{figure}' % (body, caption)

