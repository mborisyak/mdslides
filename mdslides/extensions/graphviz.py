import os
import random
import subprocess

from .extension import CodeExtension
from .image import include_picture

class GraphvizExtension(CodeExtension):
  def __init__(self, renderer, body, document, format='pdf'):
    super(GraphvizExtension, self).__init__(renderer, body)
    self.format = format

  def accepts_func(self, func):
    return func == 'graphviz'

  def __call__(self, func, args, body):
    from ..generator.utils import convert_arguments
    kwargs = convert_arguments(args)
    layout = kwargs.pop('layout', 'dot')
    assert layout in ['dot', 'neato', 'twopi', 'circo', 'fdp', 'sfdp', 'patchwork', 'osage']

    format = kwargs.pop('format', self.format)

    very_random_integer = random.randint(0, 2 ** 30)
    dot_code_file = os.path.join(
      self.renderer.working_dir,
      'dot-graph-%d.dot' % (very_random_integer, )
    )

    with open(dot_code_file, 'w') as f:
      f.write(body)

    print('Invoking %s on %s' % (layout, dot_code_file))
    renderer = subprocess.Popen(
      [layout, '-T%s' % (format, ), dot_code_file],
      cwd = self.renderer.working_dir,
      stdout=subprocess.PIPE,
      stderr=subprocess.PIPE
    )

    renderer.wait()
    stdout, stderr = renderer.communicate()

    if renderer.returncode != 0:
      raise Exception(
        '%s returned non-zero exit code:\nERRORS:\n%s\nSTDOUT:\n%s\n' % (
          layout, stderr, stdout
        )
      )

    output = stdout
    if len(output) == 0:
      raise Exception(
        '%s generated nothing:\nERRORS:\n%s\nSTDOUT:\n%s\n' % (
          layout, stderr, stdout
        )
      )

    img_file = os.path.join(
      self.renderer.working_dir,
      'dot-graph-%d.%s' % (very_random_integer, format)
    )

    with open(img_file, 'wb') as f:
      f.write(output)

    return include_picture(img_file, kwargs, self.renderer)