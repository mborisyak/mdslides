from .extension import CodeExtension

__all__ = [
  'ImageExtension',
  'include_picture',
]

class ImageExtension(CodeExtension):
  def __init__(self, renderer, code, document):
    super(ImageExtension, self).__init__(renderer, code)

  def packages(self):
    return ['graphicx']

  def render_Image(self, obj):
    from ..generator.utils import convert_arguments
    arguments = convert_arguments(obj.arguments)
    ref = get_picture(obj.ref, self.renderer.working_dir)

    return include_picture(ref, arguments, self.renderer)

def get_picture(ref, working_dir):
  import os
  import urllib.request
  import random

  path = os.path.join(working_dir, ref)

  if os.path.exists(path) and os.path.isfile(path):
    return ref
  else:
    try:
      with urllib.request.urlopen(ref) as req:
        data = req.read()

        very_random_integer = random.randint(0, 2 ** 30)
        img_path = os.path.join(working_dir, 'img_%d.png' % (very_random_integer, ))
        with open(img_path, 'wb') as f:
          f.write(data)

        return img_path
    except Exception as e:
      raise Exception('Can not find image %s on local filesystem and in the network.')

def include_picture(ref, arguments, renderer):
  includegraphics_arguments = list()
  if 'width' in arguments:
    width = arguments.pop('width')
    if isinstance(width, (int, float)):
      unit = '\\textwidth'
    else:
      unit = ''

    includegraphics_arguments.append(
      'width=%s%s' % (width, unit)
    )
  if 'height' in arguments:
    height = arguments.pop('height')
    if isinstance(height, (int, float)):
      unit = '\\textheight'
    else:
      unit = ''

    includegraphics_arguments.append(
      'height=%s%s' % (height, unit)
    )

  includegraphics_arguments = (
    '[%s]' % (', '.join(includegraphics_arguments),)
    if len(includegraphics_arguments) > 0
    else ''
  )

  img = '\\includegraphics%s{%s}' % (includegraphics_arguments, ref)

  if arguments.pop('nofigure', False):
    result = img
  else:
    caption = arguments.pop('caption', None)
    caption = ('\\caption{%s}\n' % caption) if caption is not None else ''

    label = arguments.pop('label', None)
    label = ('\\label{%s}\n' % label) if label is not None else ''

    result = \
      '\\begin{figure}\n' \
      '\\centering\n' \
      '%s\n' \
      '%s' \
      '%s' \
      '\\end{figure}\n' % (img, caption, label)

  if len(arguments) > 0:
    raise Exception(
      'Arguments are no understood: %s' % (
        ', '.join([
          '%s=%s' % (k, v) for k, v in arguments.items()
        ]),
      )
    )

  return result