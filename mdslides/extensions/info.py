import re
from .extension import CodeExtension

def get_info(code):
  kv = re.compile('(?P<key>\w+)[ \t]*[=:][ \t]*(?P<value>.*)[ \t]*([\\n]|;\\n|$)')

  try:
    return [
      (key, value)
      for key, value, sep in kv.findall(code)
    ]
  except Exception as e:
    raise Exception('Info seems to be inconsistent') from e

class InfoExtension(CodeExtension):
  def __init__(self, renderer, code=None, document=None):
    super(InfoExtension, self).__init__(renderer, code)
    if code is None:
      self.info = None
    else:
      self.info = get_info(code)

  def environment(self):
    if self.info is None:
      return dict()
    else:
      return dict(
        foreword='\n'.join([
          '\\%s{%s}' % (k, v)
          for k, v in self.info
        ])
      )

  def packages(self):
    return []

  def accepts_func(self, func):
    return False

  def __call__(self, func, args, body):
    raise NotImplementedError()