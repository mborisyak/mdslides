__all__ = [
  'CodeExtension'
]

class CodeExtension(object):
  def __init__(self, renderer, code, document=None):
    self.renderer = renderer
    self.code = code

  def packages(self):
    return []

  def accepts_func(self, func):
    return False

  def environment(self):
    return dict()

  def __call__(self, func, args, body):
    raise NotImplementedError()
