#!/bin/bash

for THEME in $(ls mdslidesthemes/)
do
  if [ "$THEME" == 'article' ]; then
    BIN='mdarticle'
  else
    BIN='mdslides'
  fi

  $BIN -t "$THEME" "mdslidesthemes/sample.md" -o "mdslidesthemes/$THEME/sample.pdf"
done