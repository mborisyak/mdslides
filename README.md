# Generate slides ~~easily~~

```info
author: mdslides
date: \today
```

```
% just a comment 
```

```minted[fontsize=\small]
% calling an extension in the preambule
% initializes this extension with arguments
% see extension section for more details.
```

```abstract
mdslides allows to rapidly make Beamer slides or LaTeX article with
beatiful equations and unleash all power of LaTeX. 
```

## Installation
### Installation

```sh[fontsize=\footnotesize]
### install a latex compiler, e.g., for ubuntu:
sudo apt-get install texlive texlive-xetex
### install mdslides
pip install git+https://gitlab.com/mborisyak/mdslides.git

### alternatively, clone the repository
git clone git@gitlab.com:mborisyak/mdslides.git
cd mdslides/ && pip install -e .
```

_mdslides_ exports two scripts: _mdslides_ and _mdarticle_,
that should be appended to _\$PATH_.


Alternatively, _mdslides_ can be invoked as:
```sh
python -m mdslides.mdslides <source>
python -m mdslides.mdarticle <source>
```

## Getting Started 
~~~
% If this looks too weird for a normal README,
% try:
% mdslides README.md
% or
% mdarticle README.md 
% and check out README.pdf
% Also, precompiled README.pdf included in the repository.
~~~

### The goal

_mdslides_ uses markdown to produce latex/beamer code.

It is designed for rapid creation of slides or articles:
- conventional markdown markup covers 90\%  of frequently used `\LaTeX` features;
- extended language of `mdslides` covers 90\%  of the rest;
  - e.g., `mdslides` allows to insert arbitrary `\LaTeX`~code, including math;
- extensions further add to `mdslides` capabilities.

_mdslides_ **does not** aim to replicate how rendered markdown documents looks ---
_.md_ files are treated as source code, however, the syntax closely follows conventional markdown.

### _mdslides_ and _markdown_

_mdslides_ uses **subset** of _markdown_ syntax and **superset** of markdown semantics:
- a particular subset of markdown is interpreted by _mdslides_ almost identically semantically;
- not all (but almost all) proper markdown files are accepted by _mdslides_:
  - for example: _mdslides_ accepts only 
    [non-closed atx-style headers](https://daringfireball.net/projects/markdown/syntax#header)
- semantics (markup) is not always preserved, for example:
  - in _markdown_, both `\_text\_` and `*text*` produce *text* (in an italic font);
  - _mdslides_ interprets `\_text\_` as `{\textbackslash}texttt` (monospace font);
- overall, _mdslides_ source files look okay when parsed by _markdown_.

### Slides and articles

_mdslides_ offers two renderers:
- beamer mode, corresponds to _mdslides_ script;
- article mode, corresponds to _mdarticle_ script.

Differences between these modes are mostly in handling *subsections* (defined by `\#\#\#`):
- beamer mode threats them as slides;
- article mode threats them as paragraphs.

### Document structure

_mdslides_ uses 3 levels of document structure:
- `\#` specifies the title of the presentation:
  - must appear only once;
  - must appear strictly at the beginning of the source file;
  - the title is supplied to the template file as _title_ variable (see section on themes).
- `\#\#` denotes section;
- `\#\#\#` denotes:
  - title of a slide in the beamer mode (_mdslides_);
  - paragraph in the article mode (_mdarticle_).

### Document structure

An example of a typical structure of a slide presentation:

```md
# Big presentation

## The first section

### The first proper slide
<some text>

### The second proper slide
<some pictures>
```

### Span elements
- `\texttt{[example](https://example.org)}`
  generates a link: [example](https://example.org) (click it);
- `\texttt{\_some text\_}` becomes `\texttt{{\textbackslash}texttt\{some text\}}`, e.g. _some text_;
- `\texttt{\_\_some text\_\_}` becomes `\texttt{{\textbackslash}underline\{some text\}}` e.g. __some text__;
- `\texttt{*some text*}` becomes `{\texttt{\textbackslash}textit\{some text\}}`, e.g. *some text*;
- `\texttt{**some text**}` becomes `\texttt{{\textbackslash}textbf\{some text\}}`, e.g. **some text**;
- `\texttt{{\textasciigrave}some code{\textasciigrave}}` inserts _some code_ as it is directly into latex:
  - `\texttt{{\textasciigrave}{\textbackslash}LaTeX{\textasciigrave}}` becomes `\LaTeX`

### Math

- `\texttt{\$x{\textasciicircum}2 = {\textbackslash}log y\$}` works just like in LaTeX: $x^2 =\log y$;
- the same for `\$\$`, e.g. `\texttt{\$\$x{\textasciicircum}2 = {\textbackslash}log y\$\$}`:
  $$x^2 =\log y$$

Alternatively, one can use blocks for math, e.g.:
```md[fontsize=\scriptsize]
~~~equation*
  \mathcal{L}(f) = \frac{1}{N} \sum^N_{i = 1} y_i \log(f(x_i));
~~~
```
becomes:
~~~equation*
  \mathcal{L}(f) = \frac{1}{N} \sum^N_{i = 1} y_i \log(f(x_i));
~~~

### Lists

Lists work just like in markdown

```md
List of stuff:
- stuff;
- thing;
- something;
```

becomes:

List of stuff:
- stuff;
- thing;
- something;

### Lists

```md
My least favorite things:
999. writing documentation;
99. understanding old code I had not documented;
18. sea food;
```

becomes:

My least favorite things:
999. writing documentation;
99. understanding old code I had not documented;
18. sea food;

### Images

Similar to markdown:

~~~
\texttt{
  \scriptsize
  ![width=1](http://imgs.xkcd.com/comics/self\_description.png)
}
~~~

results in:

![width=1](http://imgs.xkcd.com/comics/self_description.png)

### Images

Notice, that alternative text in the markdown expression for image is replaced by _arguments_:

~~~
\texttt{
  ![<arguments>](<url or path>)
}
~~~

_url or path_ must be either:
- a valid path to an image on a local filesystem:
  - path must be relative to the root directory (the one containing `\texttt{.md}` file);
- a valid url (in this case image will be downloaded).

### Images

Images accept the following arguments:
- _width, height : number or length_:
  - passed to _{\textbackslash}includegrephics_ as ... _width_ and _height_;
  - if an integer or a real number, e.g., _width=1_, width/height is multiplied by _{\textbackslash}textwidth_ or _{\textbackslash}textheight_. 
- _nofigure_:
  - _mdslides_ automatically wraps _{\textbackslash}includegrephics_ into a figure, this flag prevents that;
- _caption : string_:
  - adds _{\textbackslash}caption_.

### Footnotes

_[<ref>]_[footref] and _[<ref>]: <text>_[footdef] inserts _{\textbackslash}footnote_[footnotes], e.g.:

```md
foot[note]

[note]: about footnotes
```

becomes:

foot[note]

[note]: about footnotes

[footnotes]: Footnotes are handled by an extension, this behaviour can be overridden in favor of, e.g. _{\textbackslash}cite_ (see _references_ extension).

[footref]: Footnote reference can not appear at the beginning of a line.

[footdef]: Footnote definition **must** appear at the beginning of a line.

### Slide structure[hrules]

Markdown's horizontal rules are used by _mdslides_ to define slide structure:
- `***` (three stars) defines vertical split;
- `\textendash\textendash\textendash` (three hyphens) defines horizontal split.

Spacing between stars and hyphens specifies priority:
rules with largest number of spaces between the symbols have higher priority.

*Note:* horizontal split technically does nothing, except for defining limits of vertical splits.

[hrules]: this only applies to _beamer_ mode, article mode simply ignores horizontal rules.

### Slide structure, an example

The next slide is generated with the following layout:

```md
<upper left>
***
<upper right>

- - -

<lower>
```

Since `- - -` has more spaces in between the symbols, it has higher priority.

### A slide

A slide with text:
- and a list;
- 2-item list.

***

And another text:
1. width a list!
2. numbered one!
3. And more items than in the right one!

- - -

![width=1](http://imgs.xkcd.com/comics/self_description.png)

### Slide structure, an example

Changing priority changes layout:

```md
<left>

* * *

<upper right>
---
<lower right>
```

Now, vertical split (`* * *`) has higher priority.

### A slide

A slide with text:
- and a list;
- 2-item list.

* * *

And another text:
1. width a list!
2. numbered one!
3. And more items than in the right one!

---

![width=1](http://imgs.xkcd.com/comics/self_description.png)

## Compilation

### Compilation

The command:
```sh
bash bin/mdslides README.md
```

executed from _mdslides_' root directory outputs compiled _README.pdf_.

For the full list of options, try:
```sh
bash bin/mdslides --help
```

It is also might be convenient to add:
```sh
export PATH="$PATH:<mdslides root>/bin"
```
to your _.bashrc_:

## `\LaTeX`, blocks and more

### Blocks and code blocks

Markdown provides 2 ways for inserting code:
- via triple tildes `\textasciitilde\textasciitilde\textasciitilde`,
- via triple "graves" `\textasciigrave\textasciigrave\textasciigrave`.

_mdslides_ distinguishes between these:
- text surrounded by triple tildes is called **block** and is interpreted as `\LaTeX`~code;
- text surrounded by triple "graves" is called **code block** and denotes a call to an extension. 

### Blocks

~~~center
  \Large
  curly block = \LaTeX~code 
~~~

The full block syntax[block]:
```md
~~~<env>[<arguments>][<optional arguments>]
<body>
~~~
```
expands into:
```latex
\begin{<env>}[<optional arguments>]{<arguments>}
<body>
\end{<env>}
```
`\vspace{3mm}`
[block]: Arguments are optional --- if not specified, `\LaTeX{}` arguments are omitted as well.

### Blocks, examples

```md
~~~tabular['|c | c|']
method & ROC AUC;\\
\hline
method 1 & 0.99;\\
\hline
 method 2 & 0.91;\\
\hline
~~~
```

generates:

~~~tabular['|c | c|']
\hline
method & ROC AUC;\\
\hline
method 1 & 0.99;\\
\hline
 method 2 & 0.91;\\
\hline
~~~

### Blocks, examples

```md
~~~center
\textbf{
  \Large
  Some text!
}
~~~
```

generates:

~~~center
\textbf{
  \Large
  Some text!
}
~~~

### Blocks, examples

```md
~~~eqnarray*
f(x) &=& \sigma\big(g(x)\big);\\
g(x) &=& W \cdot x + b.
~~~
```

generates:

~~~eqnarray*
f(x) &=& \sigma\big(g(x)\big);\\
g(x) &=& W \cdot x + b.
~~~

### Blocks, examples

```md
~~~minted[python][fontsize=\large]
def inc(x):
    return x + 1
~~~
```

expands into (replace _x_ with _minted_):

```latex
\begin{x}[fontsize=\large]{python}
def inc(x):
    return x + 1
\end{x}
```

which generates:

~~~minted[python][fontsize=\large]
def inc(x):
    return x + 1
~~~

### Code blocks

~~~center
  \Large
  grave block = extension call 
~~~

The full code-block syntax [code block]:
~~~minted[md]
```<func>[<arguments>]
<body>
```
~~~

calls an extension that accepts _<func>_ passing _<arguments>_ and _<body>_ to the python code of the extension.

God only knows, what these extensions do[god].

[code block]: Arguments are optional.

[god]: For the forbidden fruit, see _extensions_ section.

### Blocks
~~~minted[md]
```<func>[<arguments>]
<body>
```
~~~

If no _<func>_ is specified, the code is inserted as is, e.g.:

~~~minted[md]
```
\begin{center}
\textbf{\Large some text!}
\end{center}
```
~~~

produces:

~~~
\begin{center}
\textbf{\Large some text!}
\end{center}
~~~

### Minted extension

[_minted_ extension](https://www.ctan.org/pkg/minted) is the lowest level extension that accepts all _<func>_, thus,
allowing _mdslides_ to emulate standard behavior of markdown, if _<func>_ is not intercepted by any other extension:

~~~minted[md]
```python
def f(x):
    return x + 1
```
~~~

is actually a call to _minted_ extension:
```python
def f(x):
    return x + 1
```

## Preamble, environments and drama

### Preamble

Block and code blocks receive a special treatment when placed after the title, but **before** any section:
- blocks define template variables;
- code blocks configure extensions.

A special code block called _preambule_ allows to configure _{\textbackslash}documentclass\{beamer\}_ class command:
- arguments of _preambule_ are passed directly to _{\textbackslash}documentclass\{beamer\}_;
- _preambule_ body is inserted before _{\textbackslash}begin\{document\}_.

### Preamble example

~~~minted[md]
# The title

```preambule[ascpectratio=169, 12pt]
\DeclareMathOperator*{\argmin}{\mathrm{arg\,min}}
\DeclareMathOperator*{\argmax}{\mathrm{arg\,max}}
```
~~~

expands into:
```latex
\documentclass[ascpectratio=169, 12pt]{beamer}
<package declaration>
\DeclareMathOperator*{\argmin}{\mathrm{arg\,min}}
\DeclareMathOperator*{\argmax}{\mathrm{arg\,max}}
```


### Environments

Each template file contains "placeholders"/environment variables (e.g., _\$title_),
which are filled with latex code upon parsing markdown. Placing blocks before the first section
allows to add custom code to environment variables, e.g.:
```md
~~~foreword
\author{A. N. Onymous}
~~~ 
```
appends _{\textbackslash}author\{A. N. Onymous\}_ to the _foreword_ variable[author]. 

[author]: See _info_ extension for an alternative way of specifying authors etc.

### Environments

List of environment variables depends on template file, but, usually, the following variables are available:
- _preamble_ --- special variable, see above;
- _foreword_ --- appears after _{\textbackslash}begin\{document\}_, but before the command for generating title;
- _preface_ --- appears after title, but before any content, absent in beamer templates;
- _afterword_ --- appears after all content.

For examples, _{\textbackslash}author\{A. N. Onymous\}_ command
should be placed into the _foreword_ because author's name is used for generating title. 

### Extension initialization

~~~center
  \Large
  before the first section = \texttt{\_\_init\_\_}\\
  after the first section = \texttt{\_\_call\_\_}\\
~~~

A code block with an name of an extension initializes that extension:
~~~minted[md]
# Title
```superextension[arg1=1, arg2, arg3=string]
some text
```
## The first section
~~~

### Extension initialization

More precisely:

~~~minted[md]
# Title
```superextension[arg1=1, arg2, arg3=string]
some text
```
## The first section
~~~

calls initialization method of _superextension_:
```python[fontsize=\small]
Extension = available_extensions['superextension']
extension = Extension(
  'some text',
  **dict(arg1=1, arg2=None, arg3='string')
)
```

### Extension initialization, example

_abstract_ extension puts _{\textbackslash}begin\{abstract\}<text>{\textbackslash}end\{abstract\}_
into the correct environment variable (_preface_):

~~~minted[md]
# Title
```abstract
Lorem ipsum dolor sit amet.
```
## The first section
~~~

initializes _abstract_  extension with the text, _abstract_ simply memorizes it,
and append it into the _preface_ variable.

### Extension initialization, example

Note, that name of an extension might not coincide with names of the functions it accepts.
For example, _minted_ extension accepts all functions, but can be configured only via:
~~~minted[md]
# Title
```minted[fontsize=\Large]
```
## The first section
~~~

### Extension initialization, example

The following code raises an error as there is no extension named _python_:
~~~minted[md]
# Title
```python[fontsize=\Large]
f = lambda x: x + 1 
```
## The first section
~~~

### Extension initialization, example

However, placed after the first section,
this code block is interpreted as _minted_ extension call,
thus, successfully renders as _python_ code with _fontsize={\textbackslash}Large_:

~~~minted[md]
# Title
## The first section
```python[fontsize=\Large]
f = lambda x: x + 1
```
~~~

```python[fontsize=\Large]
f = lambda x: x + 1
```

## Extensions

### _minted_

[_minted_ extension](https://www.ctan.org/pkg/minted) extension inserts
formatted and highlighted code.

This extension accepts all functions, but has the lowest priority,
thus catches all functions not accepted by other extensions.

Alternatively, _minted_ can be called as:

~~~minted[md]
```minted[lang=python]
f = lambda x: x + 1
```
~~~

```minted[lang=python]
f = lambda x: x + 1
```

### _minted_

Arguments passed to _minted_ extension during
initialization are wrapped into _{\textbackslash}setminted\{k=v\}_
and inserted into preamble:

~~~minted[md]
# Title
```minted[fontsize=\Large]
```
## The first section
~~~

produces:
```md
\setminted{fontsize=\Large}
```

### _info_

_info_ parses information in form of:
~~~minted[md]
```info
author: A. N. Onymous;
subtitle = just an example
date = \today
```
~~~

into:

```latex
\author{A. N. Onymous}
\subtitle{just an example}
\date{\today}
```

and insert it into _foreword_ environment variable.

### _abstract_

When initialized with a text,
_abstract_ puts _{\textbackslash}begin\{abstract\}<text>{\textbackslash}end\{abstract\}_
into _preface_:

~~~minted[md]
# Title
```abstract
Lorem ipsum dolor sit amet.
```
## The first section
~~~

Note that _preface_ is absent from beamer templates[beamer],
thus has no effect (like in these slides).

[beamer]: unless, of course, you put it there.

### _footnotes_

Handles footnotes, can be overridden by _references_.
Does not accept any arguments during initialization.
That is it\ldots

### _references_

Overrides behavior of markdown references (not default extension).

With _references_ turned on, markdown references:
```md
some statement[footnote]
[footnote]: some explanation 
```
now generate citations instead of footnotes.

### _references_

Arguments:
- _bibliography: bool_ --- if set the extension generates list of
  references via _thebibliography_, False by default.
- _style: string_ --- if _bibliography=False_ sets template for citation symbol,
  by default numbers surrounded by square brackets, e.g., _[13]_.
- _afterword: bool_ --- if set, extension automatically inserts generated references
    into _afterword_ environment variable, True by default.

### _graphviz_

Support for the [graphviz](https://www.graphviz.org/) language.
_graphviz_ executable must be installed.

~~~minted[md]
```graphviz[width=1]
digraph G {
  rankdir = LR;
  markdown -> latex [label="mdslides"];
  latex -> pdf [label="xelatex"]
}
```
~~~

```graphviz[width=1]
digraph G {
  rankdir=LR;
  markdown -> latex [label="mdslides"];
  latex -> pdf [label="xelatex"]
}
```

### _graphviz_

_graphviz_ accepts the same arguments as images, and additionally:
- _layout: string_ --- corresponds to graphviz layout (e.g., _dot_, _neato_ etc), default: _dot_;
- _format: string_ --- [format](https://graphviz.gitlab.io/_pages/doc/info/output.html)
  of the resulting image, default: _pdf_;

```graphviz[width=1, caption='An informative caption.']
digraph G {
  rankdir=LR;
  markdown -> latex [label="mdslides"];
  latex -> pdf [label="xelatex"]
}
```

### TIKZ

Inserts _tikz_ as a picture. By default wraps tikz picture into _figure_.

Accepts:
- _nofigure_ flag;
- _caption: string_ argument
- other arguments are passed into _tikz_.

***

```tikz[scale=1.5, caption='$f(x) = -x \log x$ with tikz!']
  \draw [blue, domain=0.001:1.5] plot (\x, {-\x * ln(\x)});
  \draw [thick, ->] (-0.5, 0) -- (2, 0) node[anchor=north west] {$x$};
  \draw [thick, ->] (0, -1) -- (0, 1) node[anchor=south east] {$f$};
  \node at (1, 1) {$f(x) = -x \log x$};
```

## Available themes

### Metropolis

There are 2 derivatives of [Metropolis theme](https://github.com/matze/mtheme/) available:
- _metropolis_:
  - mostly original metropolis theme;
- _msimple_:
  - simplistic black and white versions.

***

![width=1](http://i.imgur.com/Bxu52fz.png)


### Metropolis License

The theme itself is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/). This
means that if you change the theme and re-distribute it, you *must* retain the
copyright notice header and license it under the same CC-BY-SA license. This
does not affect the presentation that you create with the theme.

### Simple

If you have followed the instructions you are looking at it right now.

A minimalistic theme by Facundo Muñoz[simple]:
- [https://github.com/famuvie/beamerthemesimple](https://github.com/famuvie/beamerthemesimple)

[simple]: This repository contains a slightly modified version.

### Zurich

Clean, colorful theme.
- [https://github.com/famuvie/beamerthemesimple](https://github.com/ppletscher/beamerthemezurich)

![width=0.5](http://pletscher.org/assets/img/pletscher2012thesis-talk_small.png)

### Focus

Nice, pleasant theme from:
- [https://github.com/elauksap/focus-beamertheme](https://github.com/elauksap/focus-beamertheme)

As with _metropolis_ theme, 2 versions are available: _focus_ and _fsimple_[focus]. 

![width=0.45](https://raw.githubusercontent.com/elauksap/focus-beamertheme/master/demo-screenshots/demo-typeset.jpg)

[focus]: Well, this theme is also tweaked, as I don't like serif fonts in slides.

## Themes

### Template file

_template.tex_ is used as a \dots template for beamer or latex[template]:
- _mdslides_ parses input markdown;
- computes environment variables;
- these variables are inserted into the template.

The following variables are always present:
- _\$packages_ expands into series of `\texttt{{\textbackslash}usepackage\{<package>\}}`;
- _\$preamble_ expands into preamble;
- _\$title_ expands into whatever comes after the single \#;
- _\$document_ expands into the content of the document.

[template]: _mdslides_ uses [string.Template](https://docs.python.org/3/library/string.html#template-strings) internally.

### Template file

Other environment variables are typically present in template files:
- _foreword_ --- appears after _{\textbackslash}begin\{document\}_, but before the command for generating title;
- _preface_ --- appears after title, but before any content, absent in beamer templates;
- _afterword_ --- appears after all content.

See _template.tex_ in any of the available themes for example.

### Make your own theme

1. put theme files into _mdslides/themes/theme-name_ directory;
2. don't forget to include _template.tex_;
3. compile with _mdslides -t theme-name ..._
4. tweak fonts...
5. tweak colors...
6. crank up drama.
1. repeat...

You can also put theme files in the same directory as the source markdown file.