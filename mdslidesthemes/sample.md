# Sample presentation (or maybe article?)

```info
author: mdslides team
institute: University of Shotgun Development 
date: \today 
```

~~~preamble [aspectratio=169]
\DeclareMathOperator*{\E}{\mathbb{E}}
\DeclareMathOperator*{\argmin}{\mathrm{arg\,min}}
\DeclareMathOperator*{\argmax}{\mathrm{arg\,max}}
~~~

### Oops, slide before any section

I guess, we are doomed!

## First section

### First slide

With **a list** (!):
- first item;
- $\mathrm{inline} \cdot \mathrm{equation} = \mathrm{good}!$;
- but full equations are better:

~~~eqnarray*
  \mathcal{L} &=& -\E_{x, y} f(x, y);\\
  f(x, y) &=& y \log x + (1 - y) \log (1 - x); 
~~~

### Second slide

With columns!
- and lists;
- and *more*!
- much more...

***

And another column.
```python
def f():
  print('With CODE!')
```
- - -
`\vskip 1cm`
And a very-very-very-very-very-very-very-very-very long text...


## Summary

### Conclusion

The only possible conclusion:

~~~center
\textbf{mdslides is awesome}
~~~

- fast;
- easy;
- LaTeX.
 