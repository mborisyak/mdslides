import re
from mdslides.parser import *

def test_number():
  print()
  print(inline()('`\\\\`'))
  print(link_footnote()('[demo slides]: http://mirrors.ctan.org/macros/latex/contrib/beamer-contrib/themes/metropolis/demo/demo.pdf'))
  print(image()('![width=1](http://imgs.xkcd.com/comics/self_description.png)'))

  source = """
means that if you change the theme and re-distribute it, you *must* retain the
copyright notice header and license it under the same CC-BY-SA license. This
does not affect the presentation that you create with the theme.


[demo slides]: http://mirrors.ctan.org/macros/latex/contrib/beamer-contrib/themes/metropolis/demo/demo.pdf
[manual]: http://mirrors.ctan.org/macros/latex/contrib/beamer-contrib/themes/metropolis/doc/metropolistheme.pdf
[CTAN]: http://ctan.org/pkg/beamertheme-metropolis
"""[1:-1]

  print(rich_text()(source))

  print(argument()('flag'))
  print(real()('-191'))
  print(real()('0.99'))
  print(real()('+0.0001'))
  print(integer()('-1'))

  print('\'don\\\'t parse\'')
  print(string()('\'don\\\'t parse\''))
  print(string()('\"don\'t parse\"'))
  print(string()('r\"don\'t parse\"'))
  print(string()('word'))

  print(rich_text()('`\\texttt{[mdsldies](https://gitlab.com/mborisyak/mdslides/)}`'))
  print(argument()('key=value'))
  print(argument()('key=-1.0'))
  print(argument()('key=-1'))
  print(argument()('key=+1'))
  print(argument()('key=1.0'))

  print(doubly_starred()('**bold**'))
  print(starred()('*\\*bold\\**'))

  print(header()('### some header\n'))
  print(header()('### some *header* `with` **rich** text\n'))

  with open('../README.md', 'r') as f:
    source = f.read()

  # print(paragraph()(source).pretty())

  source = '`\\texttt{\\$preamble}` expands into preamble;'
  print(paragraph()(source).pretty())

  source = """[a=1, flag1, flag2, flag3, str='  some \\',  text']"""
  print(argument_list()(source))

  print(image()('![width=1, nofigure, caption=\'some text, for the picture, ofc.\'](imgs/pic.png)'))

  source = r"""
~~~tabular['| c | c | c | c | c |']
\hline
& non-pivoted AUC& pivoted AUC & R-score & control R-score$^\dagger$ \\
\hline
control & 0.995 &  - & 0.980 & - \\
\hline
Pivot$^\ddagger$  & - & 0.959 & 0.034 & 0.354\\
\hline
Dropout, ours$^{\dagger\dagger}$ & 0.986 & 0.980 & - & 0.299\\
\hline
Pivot, ours & 0.995 & 0.991 & 0.010 & 0.036\\
\hline
~~~
"""
  print(source)
  value = block().search(source)[-1]
  print(value)
  from mdslides.renderer import Beamer
  print(Beamer([], './').render_Block(value))

  print(image()('![width=0.7](imgs/dark-matter-dropout.pdf)'))
  print(rich_text()('![width=0.7](imgs/dark-matter-dropout.pdf)'))
  print(key_value()('width=0.7'))

  print(string()('python'))

  source = '[string.Template](https://docs.python.org/3/library/string.html#template-strings)'
  print(header().search(source))

  r = re.compile('(?<!a)bc')
  print(r.search('abc', pos=1))
  print(r.search('bc', pos=1))

  print(rich_text()('\\*text*'))
  print(rich_text()('*text\ntexts*'))
  print(rich_text()('*text\ntexts\n*'))
  print(rich_text()('*text\n\\*texts\n*'))
  print(rich_text()('*text\n\\*texts*'))
