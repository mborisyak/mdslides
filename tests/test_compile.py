import os
from mdslides import *

def test_compile():
  here = os.path.abspath(os.path.dirname(__file__))
  root = os.path.dirname(here)

  with open(os.path.join(root, 'README.md'), 'r') as f:
    source = f.read()

  with open(os.path.join(root, '__template.tex'), 'r') as f:
    template = f.read()

  parsed = document()(source)
  generator = Generator(renderer=renderers['beamer'], template=template, theme='simple')
  code = generator(parsed)

  code_path = os.path.join(here, 'test.tex')
  with open(code_path, 'w') as f:
    f.write(code)

  # compiler = Compiler()
  # compiler.compile(code_path, here)