"""
MDslides - markdown to latex/beamer parser.
"""

from setuptools import setup, find_packages
from codecs import open
import os

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
  long_description=f.read()

def walk(root):
  return [
    os.path.join(
      *os.path.join(root, item).split(os.path.sep)[1:]
    )
    for root, _, items in os.walk(root)
    for item in items
  ]

setup(
  name = 'mdslides',
  version='1.0.1',
  description="""markdown to latex/beamer generator""",
  long_description = long_description,
  url='https://gitlab.com/mborisyak/mdslides',

  author='Maxim Borisyak and contributors.',
  author_email='maximus.been@gmail.com',

  maintainer = 'Maxim Borisyak',
  maintainer_email = 'maximus.been@gmail.com',

  license='MIT',

  classifiers=[
    'Programming Language :: Python :: 3',
  ],

  keywords='markdown beamer latex',

  packages=find_packages(exclude=['contrib', 'examples', 'docs', 'tests']),

  extras_require={
    'dev': ['check-manifest'],
    'test': ['pytest>=4.0.0'],
  },

  scripts=[
    'bin/mdslides',
    'bin/mdarticle',
  ],

  package_data={
    'mdslides' : ['__template.tex'],
    'mdslidesthemes' : walk('mdslidesthemes/'),
  },
  include_package_data=True,

  install_requires=[
    'Pygments'
  ],
)


